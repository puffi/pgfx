use log::debug;
use pgfx::common::*;
use pgfx::config::Config;
use pgfx::utils::VertexBuffer;
use pgfx::{Gfx, GfxApi};
use sdl3_sys::events::{
    SDL_EventType, SDL_PollEvent, SDL_EVENT_QUIT, SDL_EVENT_WINDOW_CLOSE_REQUESTED,
    SDL_EVENT_WINDOW_RESIZED,
};
use sdl3_sys::init::{SDL_Init, SDL_Quit, SDL_INIT_VIDEO};
use sdl3_sys::properties::{
    SDL_CreateProperties, SDL_DestroyProperties, SDL_GetPointerProperty, SDL_SetBooleanProperty,
    SDL_SetNumberProperty, SDL_SetStringProperty,
};
use sdl3_sys::video::{
    SDL_CreateWindowWithProperties, SDL_DestroyWindow, SDL_GetCurrentVideoDriver,
    SDL_GetWindowProperties, SDL_GetWindowSize, SDL_GetWindowSizeInPixels,
    SDL_PROP_WINDOW_CREATE_HEIGHT_NUMBER, SDL_PROP_WINDOW_CREATE_HIGH_PIXEL_DENSITY_BOOLEAN,
    SDL_PROP_WINDOW_CREATE_RESIZABLE_BOOLEAN, SDL_PROP_WINDOW_CREATE_TITLE_STRING,
    SDL_PROP_WINDOW_CREATE_WAYLAND_CREATE_EGL_WINDOW_BOOLEAN, SDL_PROP_WINDOW_CREATE_WIDTH_NUMBER,
    SDL_PROP_WINDOW_WAYLAND_DISPLAY_POINTER, SDL_PROP_WINDOW_WAYLAND_EGL_WINDOW_POINTER,
};

fn main() {
    env_logger::init();

    assert!(unsafe { SDL_Init(SDL_INIT_VIDEO) }, "failed to init SDL");

    let props = unsafe { SDL_CreateProperties() };
    let cur_driver = unsafe { std::ffi::CStr::from_ptr(SDL_GetCurrentVideoDriver()) };
    debug!("current driver: {cur_driver:?}");
    if cur_driver == c"wayland" {
        unsafe {
            SDL_SetBooleanProperty(
                props,
                SDL_PROP_WINDOW_CREATE_WAYLAND_CREATE_EGL_WINDOW_BOOLEAN,
                true,
            );
        }
    }
    let title = c"hello-gfx";
    unsafe {
        SDL_SetStringProperty(props, SDL_PROP_WINDOW_CREATE_TITLE_STRING, title.as_ptr());
        SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_WIDTH_NUMBER, 1024);
        SDL_SetNumberProperty(props, SDL_PROP_WINDOW_CREATE_HEIGHT_NUMBER, 768);
        SDL_SetBooleanProperty(
            props,
            SDL_PROP_WINDOW_CREATE_HIGH_PIXEL_DENSITY_BOOLEAN,
            true,
        );
        SDL_SetBooleanProperty(props, SDL_PROP_WINDOW_CREATE_RESIZABLE_BOOLEAN, true);
    }

    let win = unsafe { SDL_CreateWindowWithProperties(props) };
    assert!(!win.is_null());

    unsafe {
        SDL_DestroyProperties(props);
    }

    let props = unsafe { SDL_GetWindowProperties(win) };
    let platform_handle = match unsafe {
        std::ffi::CStr::from_ptr(SDL_GetCurrentVideoDriver())
            .to_str()
            .unwrap()
    } {
        "wayland" => unsafe {
            let egl_win = SDL_GetPointerProperty(
                props,
                SDL_PROP_WINDOW_WAYLAND_EGL_WINDOW_POINTER,
                std::ptr::null_mut(),
            );
            let dpy = SDL_GetPointerProperty(
                props,
                SDL_PROP_WINDOW_WAYLAND_DISPLAY_POINTER,
                std::ptr::null_mut(),
            );
            PlatformHandle::Wayland {
                display: dpy,
                egl_window: egl_win,
            }
        },
        _ => panic!("unsupported video driver"),
    };
    {
        let mut width = 0;
        let mut height = 0;
        unsafe {
            SDL_GetWindowSize(win, &mut width as _, &mut height as _);
        }
        let mut width_pixels = 0;
        let mut height_pixels = 0;
        unsafe {
            SDL_GetWindowSizeInPixels(win, &mut width_pixels as _, &mut height_pixels as _);
        }

        let mut gfx = Gfx::new_with_config(
            platform_handle,
            WindowSize {
                width,
                height,
                width_pixels,
                height_pixels,
            },
            Config {
                request_debug_context: true,
                ..Default::default()
            },
        )
        .unwrap();

        let mut vertex_buffer = VertexBuffer::new(64 * 1024, &mut gfx);
        let quad = [
            Vertex {
                pos: [-0.5, -0.5],
                uv: [0.0, 0.0],
            },
            Vertex {
                pos: [0.5, -0.5],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.5, 0.5],
                uv: [1.0, 1.0],
            },
            Vertex {
                pos: [-0.5, 0.5],
                uv: [0.0, 1.0],
            },
        ];
        let quad_indices = [0, 1, 2, 2, 3, 0];

        let triangle = [
            Vertex {
                pos: [-0.5, -0.5],
                uv: [0.0, 0.0],
            },
            Vertex {
                pos: [0.5, -0.5],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.0, 0.5],
                uv: [0.5, 1.0],
            },
        ];
        let triangle_indices = [0, 1, 2];

        let quad_vertex_bytes = unsafe { quad.align_to::<u8>().1 };
        let quad_index_bytes = unsafe { quad_indices.align_to::<u8>().1 };
        let triangle_vertex_bytes = unsafe { triangle.align_to::<u8>().1 };
        let triangle_index_bytes = unsafe { triangle_indices.align_to::<u8>().1 };

        let quad_alloc = vertex_buffer
            .allocate_mesh(
                quad_index_bytes.len() as u32,
                quad_vertex_bytes.len() as u32,
                16,
                false,
            )
            .unwrap();
        let triangle_alloc = vertex_buffer
            .allocate_mesh(
                triangle_index_bytes.len() as u32,
                triangle_vertex_bytes.len() as u32,
                16,
                false,
            )
            .unwrap();

        //let vertices = unsafe { [-0.5f32, -0.5f32, 0.5f32, -0.5f32, 0.0f32, 0.5f32].align_to::<u8>().1.to_vec() };
        //let indices = unsafe { [0u32, 1, 2].align_to::<u8>().1.to_vec() };
        //let vertex_buffer = gfx.create_storage_buffer(40);
        //gfx.update_storage_buffer_offset(vertex_buffer, &indices, 0);
        //gfx.update_storage_buffer_offset(vertex_buffer, &vertices, 16);
        let draw_infos = [
            DrawInfo {
                index_offset: quad_alloc.index_bytes.offset / 4,
                vertex_offset: quad_alloc.vertex_bytes.offset / quad_alloc.vertex_alignment,
            },
            DrawInfo {
                index_offset: triangle_alloc.index_bytes.offset / 4,
                vertex_offset: triangle_alloc.vertex_bytes.offset / triangle_alloc.vertex_alignment,
            },
        ];
        let draw_info_bytes = unsafe { draw_infos.align_to::<u8>().1 };
        let draw_info_buffer = gfx.create_storage_buffer(draw_info_bytes.len() as u32);
        gfx.update_storage_buffer(draw_info_buffer, draw_info_bytes);

        vertex_buffer.upload(&quad_alloc, quad_index_bytes, quad_vertex_bytes, &mut gfx);
        vertex_buffer.upload(
            &triangle_alloc,
            triangle_index_bytes,
            triangle_vertex_bytes,
            &mut gfx,
        );
        //gfx.update_storage_buffer_offset(vertex_buffer.buffer(), quad_index_bytes, quad_alloc.index_bytes.offset);
        //gfx.update_storage_buffer_offset(vertex_buffer.buffer(), quad_vertex_bytes, quad_alloc.vertex_bytes.offset);
        //gfx.update_storage_buffer_offset(vertex_buffer.buffer(), triangle_index_bytes, triangle_alloc.index_bytes.offset);
        //gfx.update_storage_buffer_offset(vertex_buffer.buffer(), triangle_vertex_bytes, triangle_alloc.vertex_bytes.offset);

        let vs = gfx.create_vertex_shader(VS.as_bytes());
        let fs = gfx.create_fragment_shader(FS.as_bytes());

        let mut quit = false;
        while !quit {
            gfx.clear_render_target_att_cv(
                DEFAULT_RENDER_TARGET,
                Attachment::Color0,
                ClearValue::Vec4([0.3, 0.3, 0.3, 1.0]),
            );
            gfx.set_storage_buffer(draw_info_buffer, 0);
            gfx.set_storage_buffer(vertex_buffer.buffer(), 1);
            gfx.set_storage_buffer(vertex_buffer.buffer(), 2);
            gfx.set_vertex_shader(vs);
            gfx.set_fragment_shader(fs);
            gfx.set_render_targets(DEFAULT_RENDER_TARGET, DEFAULT_RENDER_TARGET);
            gfx.draw_instances_base(6, 1, 0);
            gfx.draw_instances_base(3, 1, 1);
            gfx.present(PresentMode::Vsync);

            let mut evt = unsafe { std::mem::MaybeUninit::zeroed().assume_init() };
            while unsafe { SDL_PollEvent(&mut evt) } {
                unsafe {
                    match SDL_EventType(evt.r#type) {
                        SDL_EVENT_QUIT => quit = true,
                        SDL_EVENT_WINDOW_CLOSE_REQUESTED => quit = true,
                        SDL_EVENT_WINDOW_RESIZED => {
                            let width = evt.window.data1;
                            let height = evt.window.data2;
                            let mut width_pixels = width;
                            let mut height_pixels = height;
                            SDL_GetWindowSizeInPixels(win, &mut width_pixels, &mut height_pixels);
                            let window_size = WindowSize {
                                width,
                                height,
                                width_pixels,
                                height_pixels,
                            };
                            gfx.update_window_size(window_size);
                            gfx.set_viewport(Viewport {
                                x: 0,
                                y: 0,
                                width: width_pixels,
                                height: height_pixels,
                            });
                            // update gfx window size
                            // set viewport
                        }
                        _ => {}
                    }
                }
            }
        }
    }

    unsafe {
        SDL_DestroyWindow(win);
        SDL_Quit();
    }
}

const VS: &'static str = "
#version 460 core

layout(location = 0) out vec2 o_uv;

struct DrawInfo
{
    uint index_offset;
    uint vertex_offset;
};

struct Vertex
{
    vec2 pos;
    vec2 uv;
};

layout(std430, binding = 0) buffer DrawInfos
{
    DrawInfo draw_infos[];
};

layout(std430, binding = 1) buffer Indices
{
    uint indices[];
};

layout(std430, binding = 2) buffer Vertices
{
    Vertex vertices[];
};

out gl_PerVertex
{
    vec4 gl_Position;
};

const vec2 VERTICES[3] = {
    vec2(-0.5, -0.5),
    vec2(0.5, -0.5),
    vec2(0.0, 0.5),
};

void main()
{
    //uint instance = gl_InstanceID + gl_BaseInstance;
    //uint index_offset = draw_infos[instance].index_offset;
    //uint vertex_offset = draw_infos[instance].vertex_offset;
    //uint index = indices[index_offset + gl_VertexID];
    //gl_Position = vec4(vertices[vertex_offset + index].pos, 0, 1);
    //gl_Position = vec4(VERTICES[gl_VertexID], 0, 1);
    
    uint instance = gl_InstanceID + gl_BaseInstance;
    DrawInfo info = draw_infos[instance];
    uint index = indices[info.index_offset + gl_VertexID];
    Vertex vertex = vertices[info.vertex_offset + index];

    gl_Position = vec4(vertex.pos, 0, 1) + (vec4(0.375, 0.5, 0, 0) * gl_BaseInstance);
    o_uv = vertex.uv;
}
";

const FS: &'static str = "
#version 460 core

layout(location = 0) in vec2 uv;
layout(location = 0) out vec4 o_col;

void main()
{
    o_col = vec4(uv, 0, 1);
}
";

struct Vertex {
    pos: [f32; 2],
    uv: [f32; 2],
}

struct DrawInfo {
    index_offset: u32,
    vertex_offset: u32,
}
