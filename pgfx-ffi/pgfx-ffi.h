#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



/**
 * An enum representing the available verbosity levels of the logger.
 *
 * Typical usage includes: checking if a certain `Level` is enabled with
 * [`log_enabled!`](macro.log_enabled.html), specifying the `Level` of
 * [`log!`](macro.log.html), and comparing a `Level` directly to a
 * [`LevelFilter`](enum.LevelFilter.html).
 */
enum Level {
  /**
   * The "error" level.
   *
   * Designates very serious errors.
   */
  Error = 1,
  /**
   * The "warn" level.
   *
   * Designates hazardous situations.
   */
  Warn,
  /**
   * The "info" level.
   *
   * Designates useful information.
   */
  Info,
  /**
   * The "debug" level.
   *
   * Designates lower priority information.
   */
  Debug,
  /**
   * The "trace" level.
   *
   * Designates very low priority, often extremely verbose, information.
   */
  Trace,
};
typedef uintptr_t Level;

typedef struct _gfx_t {

} _gfx_t;

typedef struct _gfx_t *gfx_t;

typedef enum PlatformHandle_Tag {
  None,
  Wayland,
  Xlib,
  Win32,
} PlatformHandle_Tag;

typedef struct Wayland_Body {
  void *surface;
  void *display;
  void *egl_window;
} Wayland_Body;

typedef struct Xlib_Body {
  unsigned long window;
  void *display;
} Xlib_Body;

typedef struct Win32_Body {
  void *hwnd;
  void *hdc;
  void *hinstance;
} Win32_Body;

typedef struct PlatformHandle {
  PlatformHandle_Tag tag;
  union {
    Wayland_Body wayland;
    Xlib_Body xlib;
    Win32_Body win32;
  };
} PlatformHandle;

typedef struct PlatformHandle gfx_platform_handle_t;

typedef struct Config {
  uintptr_t max_vertex_buffers;
  uintptr_t max_index_buffers;
  uintptr_t max_attributes;
  uintptr_t vertex_buffer_size;
  uintptr_t max_vertex_shaders;
  uintptr_t max_fragment_shaders;
  uintptr_t max_render_targets;
  uintptr_t max_textures;
  uintptr_t max_bound_textures;
  uintptr_t max_samplers;
  uintptr_t max_uniform_buffers;
  uintptr_t max_bound_uniform_buffers;
  uintptr_t max_uniform_buffer_size;
  uintptr_t max_storage_buffers;
  uintptr_t max_bound_storage_buffers;
  uintptr_t max_storage_buffer_size;
  uintptr_t staging_buffer_size;
} Config;













gfx_t gfx_create(gfx_platform_handle_t platform_handle);

gfx_t gfx_create_with_config(struct PlatformHandle platform_handle, struct Config config);

void gfx_destroy(gfx_t gfx);
