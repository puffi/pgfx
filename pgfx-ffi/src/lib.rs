#![allow(non_camel_case_types)]

use pgfx::Gfx;

use pgfx::GfxApi;
use tracing::error;

#[repr(C)]
pub struct _gfx_t {}
type gfx_t = *mut _gfx_t;
type gfx_platform_handle_t = pgfx::common::PlatformHandle;

#[no_mangle]
pub extern "C" fn gfx_create(platform_handle: gfx_platform_handle_t) -> gfx_t {
    gfx_create_with_config(platform_handle, pgfx::config::Config::default())
}

#[no_mangle]
pub extern "C" fn gfx_create_with_config(
    platform_handle: pgfx::common::PlatformHandle,
    config: pgfx::config::Config,
) -> gfx_t {
    let gfx = match Gfx::new_with_config(platform_handle, config) {
        Ok(gfx) => gfx,
        Err(err) => {
            error!("Failed to create Gfx instance: {:?}", err);
            return std::ptr::null_mut();
        }
    };
    let gfx = Box::new(gfx);
    let ptr = Box::into_raw(gfx);
    ptr as gfx_t
}

#[no_mangle]
pub extern "C" fn gfx_destroy(gfx: gfx_t) {
    match gfx.into() {
        Some(gfx) => {
            let _ = unsafe { Box::from_raw(gfx as *mut Gfx) };
        }
        None => {}
    }
}
