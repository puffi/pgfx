#![allow(non_snake_case)]

use std::sync::OnceLock;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(C)]
pub struct Config {
    pub max_vertex_buffers: usize,
    pub max_index_buffers: usize,
    pub max_attributes: usize,
    pub vertex_buffer_size: usize,
    pub max_vertex_shaders: usize,
    pub max_fragment_shaders: usize,
    pub max_render_targets: usize,
    pub max_textures: usize,
    //pub max_bound_textures: usize,
    pub max_samplers: usize,
    pub max_uniform_buffers: usize,
    pub max_bound_uniform_buffers: usize,
    pub max_uniform_buffer_size: usize,
    pub max_storage_buffers: usize,
    pub max_bound_storage_buffers: usize,
    pub max_storage_buffer_size: usize,
    pub staging_buffer_size: usize,
    pub use_reverse_z: bool,
    pub request_debug_context: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            max_vertex_buffers: 4096,
            max_index_buffers: 4096,
            max_attributes: 16,
            vertex_buffer_size: 128 * 1024 * 1024,
            max_vertex_shaders: 2048,
            max_fragment_shaders: 2048,
            max_render_targets: 64,
            max_textures: 256,
            //max_bound_textures: 8,
            max_samplers: 4096,
            max_uniform_buffers: 4096,
            max_bound_uniform_buffers: 8,
            max_uniform_buffer_size: 64 * 1024,
            max_storage_buffers: 32,
            max_bound_storage_buffers: 8,
            max_storage_buffer_size: 128 * 1024 * 1024,
            staging_buffer_size: 8 * 1024 * 1024,
            use_reverse_z: false,
            request_debug_context: false,
        }
    }
}

static CONFIG: OnceLock<Config> = OnceLock::new();

pub(crate) fn init_config(config: Config) -> bool {
    CONFIG.set(config).is_ok()
}

pub fn MAX_VERTEX_BUFFERS() -> usize {
    CONFIG.get().unwrap().max_vertex_buffers
}

pub fn MAX_INDEX_BUFFERS() -> usize {
    CONFIG.get().unwrap().max_index_buffers
}

pub fn MAX_ATTRIBUTES() -> usize {
    CONFIG.get().unwrap().max_attributes
}

pub fn VERTEX_BUFFER_SIZE() -> usize {
    CONFIG.get().unwrap().vertex_buffer_size
}

pub fn MAX_VERTEX_SHADERS() -> usize {
    CONFIG.get().unwrap().max_vertex_shaders
}

pub fn MAX_FRAGMENT_SHADERS() -> usize {
    CONFIG.get().unwrap().max_fragment_shaders
}

pub fn MAX_RENDER_TARGETS() -> usize {
    CONFIG.get().unwrap().max_render_targets
}

pub const fn DEFAULT_RENDER_TARGET_INDEX() -> usize {
    0
}

pub fn MAX_TEXTURES() -> usize {
    CONFIG.get().unwrap().max_textures
}

//pub fn MAX_BOUND_TEXTURES() -> usize {
//    CONFIG.get().unwrap().max_bound_textures
//}

pub fn MAX_SAMPLERS() -> usize {
    CONFIG.get().unwrap().max_samplers
}

pub fn MAX_UNIFORM_BUFFERS() -> usize {
    CONFIG.get().unwrap().max_uniform_buffers
}

pub fn MAX_BOUND_UNIFORM_BUFFERS() -> usize {
    CONFIG.get().unwrap().max_bound_uniform_buffers
}

pub fn MAX_UNIFORM_BUFFER_SIZE() -> usize {
    CONFIG.get().unwrap().max_uniform_buffer_size
}

pub fn MAX_STORAGE_BUFFERS() -> usize {
    CONFIG.get().unwrap().max_storage_buffers
}

pub fn MAX_BOUND_STORAGE_BUFFERS() -> usize {
    CONFIG.get().unwrap().max_bound_storage_buffers
}

pub fn MAX_STORAGE_BUFFER_SIZE() -> usize {
    CONFIG.get().unwrap().max_storage_buffer_size
}

pub fn STAGING_BUFFER_SIZE() -> usize {
    CONFIG.get().unwrap().staging_buffer_size
}

pub fn USE_REVERSE_Z() -> bool {
    CONFIG.get().unwrap().use_reverse_z
}

pub fn REQUEST_DEBUG_CONTEXT() -> bool {
    CONFIG.get().unwrap().request_debug_context
}
