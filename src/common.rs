use std::{
    ffi::{c_ulong, c_void},
    fmt::Display,
};

use paste::paste;

use super::config;

pub enum PlatformHandle {
    None,
    Wayland {
        display: *mut c_void,
        egl_window: *mut c_void,
    },
    Xlib {
        window: c_ulong,
        display: *mut c_void,
    },
    Win32 {
        hwnd: *mut c_void,
        hdc: *mut c_void,
        hinstance: *mut c_void,
    },
}

macro_rules! define_handle {
    ($name: ident, $type: ty, $max: expr) => {
        paste! {
            #[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
            pub struct [<$name Handle>] {
                id: $type,
            }

            impl [<$name Handle>] {
                pub const fn new(id: $type) -> Self {
                    Self { id }
                }

                pub fn is_valid(&self) -> bool {
                    self.id != $max as _
                }

                pub fn get_id(&self) -> $type {
                    self.id
                }

                pub fn invalid() -> Self {
                    Default::default()
                }
            }

            impl Default for [<$name Handle>] {
                fn default() -> Self {
                    Self {
                        id: $type::MAX,
                    }
                }
            }
        }
    };
}

macro_rules! define_raw_handle {
    ($name: ident, $type: ty, $invalid: expr) => {
        paste! {
            #[repr(transparent)]
            #[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
            pub struct [<$name Handle>] {
                handle: $type,
            }

            impl [<$name Handle>] {
                pub const fn new(handle: $type) -> Self {
                    Self { handle }
                }

                pub fn is_valid(&self) -> bool {
                    self.handle != $invalid as _
                }

                pub fn get_raw_handle(&self) -> $type {
                    self.handle
                }

                pub fn invalid() -> Self {
                    Default::default()
                }
            }

            impl Default for [<$name Handle>] {
                fn default() -> Self {
                    Self { handle: $invalid }
                }
            }
        }
    };
}

define_handle!(RenderTarget, u16, config::MAX_RENDER_TARGETS());
define_handle!(VertexShader, u16, config::MAX_VERTEX_SHADERS());
define_handle!(FragmentShader, u16, config::MAX_FRAGMENT_SHADERS());
define_handle!(VertexBuffer, u16, config::MAX_VERTEX_BUFFERS());
define_handle!(IndexBuffer, u16, config::MAX_INDEX_BUFFERS());
define_handle!(UniformBuffer, u16, config::MAX_UNIFORM_BUFFERS());
define_handle!(StorageBuffer, u16, config::MAX_STORAGE_BUFFERS());
define_handle!(Texture, u16, config::MAX_TEXTURES());

define_raw_handle!(BindlessTexture, usize, 0usize);

pub const DEFAULT_RENDER_TARGET: RenderTargetHandle =
    RenderTargetHandle::new(config::DEFAULT_RENDER_TARGET_INDEX() as _);
/// Blit State in shader:
/// layout(std430, binding = BLIT_STATE_BINDING_LOCATION) readonly buffer BlitState
/// {
///     vec2 vertices[3];
///     layout(bindless_sampler) sampler2D textures[]; // provided attachments in that order, up to 11
/// };
pub const BLIT_STATE_BINDING_LOCATION: u32 = 0;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum PresentMode {
    Immediate,
    Vsync,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Attachment {
    Color0,
    Color1,
    Color2,
    Color3,
    Color4,
    Color5,
    Color6,
    Color7,
    Depth,
    Stencil,
    DepthStencil,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ClearValue {
    F32(f32),
    I32(i32),
    U32(u32),
    Vec4([f32; 4]),
    F32I32(f32, i32),
    None,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum IndexType {
    U16,
    U32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
#[repr(u8)]
pub enum AttributeType {
    #[default]
    None,
    U8,
    F32,
    I32,
    U32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Attribute {
    pub location: u8,
    pub ty: AttributeType,
    pub num: u8,
    pub normalize: bool,
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct Size {
    pub width: u32,
    pub height: u32,
    pub depth: u32,
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct Offset {
    pub x: u32,
    pub y: u32,
    pub z: u32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Format {
    R8,
    RGB8,
    RGBA8,
    RGB16F,
    RGBA16F,
    RGB32F,
    RGBA32F,
    Depth32,
    Depth32Stencil8,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Filter {
    Nearest = 0,
    Linear = 1,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Wrap {
    Repeat = 0,
    MirroredRepeat = 1,
    Clamp = 2,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AnisotropicFilter {
    None = 0,
    X4 = 1,
    X8 = 2,
    X16 = 3,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum TextureType {
    _2D,
}

pub(crate) const FILTER_BITS: u16 = 1;
pub(crate) const FILTER_SHIFT_MIN: u16 = 0;
pub(crate) const FILTER_MASK_MIN: u16 = 1 << FILTER_SHIFT_MIN;
pub(crate) const FILTER_SHIFT_MAG: u16 = FILTER_SHIFT_MIN + FILTER_BITS;
pub(crate) const FILTER_MASK_MAG: u16 = 1 << FILTER_SHIFT_MAG;
pub(crate) const ANISOTROPIC_FILTER_BITS: u16 = 2;
pub(crate) const ANISOTROPIC_FILTER_SHIFT: u16 = FILTER_SHIFT_MAG + FILTER_BITS;
pub(crate) const ANISOTROPIC_FILTER_MASK: u16 = 3 << ANISOTROPIC_FILTER_SHIFT;
pub(crate) const WRAP_BITS: u16 = 2;
pub(crate) const WRAP_SHIFT_U: u16 = ANISOTROPIC_FILTER_SHIFT + ANISOTROPIC_FILTER_BITS;
pub(crate) const WRAP_MASK_U: u16 = 3 << WRAP_SHIFT_U;
pub(crate) const WRAP_SHIFT_V: u16 = WRAP_SHIFT_U + WRAP_BITS;
pub(crate) const WRAP_MASK_V: u16 = 3 << WRAP_SHIFT_V;
pub(crate) const WRAP_SHIFT_W: u16 = WRAP_SHIFT_V + WRAP_BITS;
pub(crate) const WRAP_MASK_W: u16 = 3 << WRAP_SHIFT_W;
pub(crate) const SAMPLER_BITS: u16 = WRAP_SHIFT_W + WRAP_BITS;
pub(crate) const MAX_SAMPLERS: u16 = 2u16.pow(SAMPLER_BITS as u32);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Sampler {
    bits: u16,
}

impl Default for Sampler {
    fn default() -> Self {
        Self::new()
    }
}

impl Sampler {
    pub fn new() -> Self {
        Self { bits: 0 }
    }

    pub fn with_filter(mut self, filter: Filter) -> Self {
        self.set_min_filter(filter);
        self.set_mag_filter(filter);
        self
    }

    pub fn with_min_filter(mut self, filter: Filter) -> Self {
        self.set_min_filter(filter);
        self
    }

    pub fn with_mag_filter(mut self, filter: Filter) -> Self {
        self.set_mag_filter(filter);
        self
    }

    pub fn with_anisotropic_filter(mut self, filter: AnisotropicFilter) -> Self {
        self.set_anisotropic_filter(filter);
        self
    }

    pub fn with_wrap(mut self, wrap: Wrap) -> Self {
        self.set_wrap_u(wrap);
        self.set_wrap_v(wrap);
        self.set_wrap_w(wrap);
        self
    }

    pub fn with_wrap_u(mut self, wrap: Wrap) -> Self {
        self.set_wrap_u(wrap);
        self
    }

    pub fn with_wrap_v(mut self, wrap: Wrap) -> Self {
        self.set_wrap_v(wrap);
        self
    }

    pub fn with_wrap_w(mut self, wrap: Wrap) -> Self {
        self.set_wrap_w(wrap);
        self
    }

    pub fn set_min_filter(&mut self, filter: Filter) {
        self.bits &= !FILTER_MASK_MIN;
        self.bits |= (filter as u16) << FILTER_SHIFT_MIN;
    }

    pub fn set_mag_filter(&mut self, filter: Filter) {
        self.bits &= !FILTER_MASK_MAG;
        self.bits |= (filter as u16) << FILTER_SHIFT_MAG;
    }

    pub fn set_anisotropic_filter(&mut self, filter: AnisotropicFilter) {
        self.bits &= !ANISOTROPIC_FILTER_MASK;
        self.bits |= (filter as u16) << ANISOTROPIC_FILTER_SHIFT;
    }

    pub fn set_wrap_u(&mut self, wrap: Wrap) {
        self.bits &= !WRAP_MASK_U;
        self.bits |= (wrap as u16) << WRAP_SHIFT_U;
    }

    pub fn set_wrap_v(&mut self, wrap: Wrap) {
        self.bits &= !WRAP_MASK_V;
        self.bits |= (wrap as u16) << WRAP_SHIFT_V;
    }

    pub fn set_wrap_w(&mut self, wrap: Wrap) {
        self.bits &= !WRAP_MASK_W;
        self.bits |= (wrap as u16) << WRAP_SHIFT_W;
    }

    pub fn get_hash(&self) -> u16 {
        self.bits
    }

    pub fn get_min_filter(&self) -> Filter {
        match (self.bits & FILTER_MASK_MIN) >> FILTER_SHIFT_MIN {
            0 => Filter::Nearest,
            1 => Filter::Linear,
            _ => unreachable!(),
        }
    }

    pub fn get_mag_filter(&self) -> Filter {
        match (self.bits & FILTER_MASK_MAG) >> FILTER_SHIFT_MAG {
            0 => Filter::Nearest,
            1 => Filter::Linear,
            _ => unreachable!(),
        }
    }

    pub fn get_anisotropic_filter(&self) -> AnisotropicFilter {
        match (self.bits & ANISOTROPIC_FILTER_MASK) >> ANISOTROPIC_FILTER_SHIFT {
            0 => AnisotropicFilter::None,
            1 => AnisotropicFilter::X4,
            2 => AnisotropicFilter::X8,
            3 => AnisotropicFilter::X16,
            _ => unreachable!(),
        }
    }

    pub fn get_wrap_u(&self) -> Wrap {
        match (self.bits & WRAP_MASK_U) >> WRAP_SHIFT_U {
            0 => Wrap::Repeat,
            1 => Wrap::MirroredRepeat,
            2 => Wrap::Clamp,
            _ => unreachable!(),
        }
    }

    pub fn get_wrap_v(&self) -> Wrap {
        match (self.bits & WRAP_MASK_V) >> WRAP_SHIFT_V {
            0 => Wrap::Repeat,
            1 => Wrap::MirroredRepeat,
            2 => Wrap::Clamp,
            _ => unreachable!(),
        }
    }

    pub fn get_wrap_w(&self) -> Wrap {
        match (self.bits & WRAP_MASK_W) >> WRAP_SHIFT_W {
            0 => Wrap::Repeat,
            1 => Wrap::MirroredRepeat,
            2 => Wrap::Clamp,
            _ => unreachable!(),
        }
    }
}

impl Display for Sampler {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Sampler {{ min: {:?}, mag: {:?}, anisotropic: {:?}, wrap_u: {:?}, wrap_v: {:?}, wrap_w: {:?} }}", self.get_min_filter(), self.get_mag_filter(), self.get_anisotropic_filter(), self.get_wrap_u(), self.get_wrap_v(), self.get_wrap_w())
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct BlendState {
    pub func: BlendFunc,
    pub equation: BlendEquation,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct BlendFunc {
    pub source: BlendFactor,
    pub destination: BlendFactor,
}

impl Default for BlendFunc {
    fn default() -> Self {
        Self {
            source: BlendFactor::One,
            destination: BlendFactor::Zero,
        }
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BlendFactor {
    Zero,
    One,
    SrcColor,
    OneMinusSrcColor,
    DstColor,
    OneMinusDstColor,
    SrcAlpha,
    OneMinusSrcAlpha,
    DstAlpha,
    OneMinusDstAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SrcAlphaSaturate,
    Src1Color,
    OneMinusSrc1Color,
    // 	src1Alpha,
    OneMinusSrc1Alpha,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BlendEquation {
    Add,
    Subtract,
    ReverseSubtract,
    Min,
    Max,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct DepthState {
    pub func: DepthFunc,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy, Default)]
pub enum DepthFunc {
    Never,
    #[default]
    Less,
    Equal,
    LEqual,
    Greater,
    NotEqual,
    GEqual,
    Always,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct CullState {
    pub mode: CullMode,
    pub front_face: CullFrontFace,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum CullMode {
    Front,
    Back,
    FrontAndBack,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum CullFrontFace {
    Cw,
    Ccw,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct ScissorState {
    pub x: u32,
    pub y: u32,
    pub width: u32,
    pub height: u32,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct Viewport {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct WindowSize {
    pub width: i32,
    pub height: i32,
    pub width_pixels: i32,
    pub height_pixels: i32,
}

#[derive(Debug, Clone)]
pub struct RenderTargetDesc<'a> {
    pub attachments: &'a [Attachment],
    pub formats: &'a [Format],
    pub clear_values: &'a [ClearValue],
    pub texture_sizes: &'a [RenderTextureSize],
}

#[derive(Debug, Clone, Copy)]
pub enum RenderTextureSize {
    Scaled { width: f32, height: f32 },
    Fixed { width: u32, height: u32 },
}
