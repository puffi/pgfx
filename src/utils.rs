use log::{debug, error};

use crate::{common::StorageBufferHandle, Gfx, GfxApi};

pub struct VertexBuffer {
    buffer: StorageBufferHandle,
    allocations: Vec<Allocation>,
    free_list: Vec<BufferSpace>,
    temp_allocations: Vec<Allocation>,

    head: u32,
    tail: u32,
    total_size: u32,
}

impl VertexBuffer {
    pub fn new(size: u32, gfx: &mut Gfx) -> Self {
        let buffer = gfx.create_storage_buffer(size);
        assert!(
            buffer.is_valid(),
            "failed to create vertex buffer storage buffer"
        );
        Self {
            buffer,
            allocations: Vec::new(),
            free_list: Vec::new(),
            temp_allocations: Vec::new(),

            head: 0,
            tail: size,
            total_size: size,
        }
    }

    pub fn total_size(&self) -> u32 {
        self.total_size
    }

    pub fn available_size(&self) -> u32 {
        self.tail - self.head
    }

    pub fn buffer(&self) -> StorageBufferHandle {
        self.buffer
    }

    pub fn allocate_mesh(
        &mut self,
        index_bytes: u32,
        vertex_bytes: u32,
        vertex_alignment: u32,
        temporary: bool,
    ) -> Option<Allocation> {
        if temporary {
            self.allocate_temp(index_bytes, vertex_bytes, vertex_alignment)
        } else {
            self.allocate_perm(index_bytes, vertex_bytes, vertex_alignment)
        }
    }

    pub fn upload(&self, allocation: &Allocation, indices: &[u8], vertices: &[u8], gfx: &mut Gfx) {
        gfx.update_storage_buffer_offset(self.buffer, indices, allocation.index_bytes.offset);
        gfx.update_storage_buffer_offset(self.buffer, vertices, allocation.vertex_bytes.offset);
    }

    pub fn free_temporary(&mut self) {
        self.tail = self.total_size;
    }

    pub fn free_allocation(&mut self, allocation: Allocation) {
        if let Some((idx, alloc)) = self
            .allocations
            .iter()
            .enumerate()
            .find(|(_idx, val)| val.buffer_space == allocation.buffer_space)
        {
            self.free_list.push(alloc.buffer_space);
            self.allocations.remove(idx);
            self.optimize_freespace();
        }
    }

    fn optimize_freespace(&mut self) {
        self.free_list
            .sort_by(|lhs, rhs| rhs.offset.cmp(&lhs.offset));
        for i in (0..self.free_list.len()).rev() {
            let space = self.free_list[i];
            if (space.offset + space.size) == self.head {
                self.free_list.remove(i);
                self.head -= space.size;
            } else {
                break;
            }
        }
    }

    fn allocate_perm(
        &mut self,
        index_bytes: u32,
        vertex_bytes: u32,
        vertex_alignment: u32,
    ) -> Option<Allocation> {
        let mut allocation = None;
        let mut to_remove = Vec::new();
        for (idx, space) in self.free_list.iter_mut().enumerate() {
            let ib_alloc = match Self::allocate_front(space.offset, space.size, index_bytes, 4) {
                Some(alloc) => alloc,
                None => {
                    debug!("[VertexBuffer]: freelist space {} does not have enough space for index data", idx);
                    continue;
                }
            };

            let vb_alloc = match Self::allocate_front(
                space.offset + ib_alloc.size,
                space.size - ib_alloc.size,
                vertex_bytes,
                vertex_alignment,
            ) {
                Some(alloc) => alloc,
                None => {
                    debug!("[VertexBuffer]: freelist space {} does not have enough space for vertex data", idx);
                    continue;
                }
            };

            allocation = Some(Allocation {
                buffer_space: BufferSpace {
                    offset: ib_alloc.offset,
                    size: ib_alloc.size + vb_alloc.size,
                },
                index_bytes: BufferSpace {
                    offset: ib_alloc.data_offset,
                    size: ib_alloc.data_length,
                },
                vertex_bytes: BufferSpace {
                    offset: vb_alloc.data_offset,
                    size: vb_alloc.data_length,
                },
                vertex_alignment,
            });

            space.size -= ib_alloc.size + vb_alloc.size;
            if space.size == 0 {
                to_remove.push(idx);
                break;
            }
        }
        for idx in to_remove.drain(..).rev() {
            self.free_list.remove(idx);
        }
        if allocation.is_some() {
            return allocation;
        }

        let available_size = self.available_size();

        let ib_alloc = match Self::allocate_front(self.head, available_size, index_bytes, 4) {
            Some(alloc) => alloc,
            None => {
                error!(
                    "[VertexBuffer]: available space {} does not have enough space for index data",
                    available_size
                );
                return None;
            }
        };
        let vb_alloc = match Self::allocate_front(
            self.head + ib_alloc.size,
            available_size - ib_alloc.size,
            vertex_bytes,
            vertex_alignment,
        ) {
            Some(alloc) => alloc,
            None => {
                error!(
                    "[VertexBuffer]: available space {} does not have enough space for vertex data",
                    available_size
                );
                return None;
            }
        };

        let allocation_size = ib_alloc.size + vb_alloc.size;
        self.head += allocation_size;
        allocation = Some(Allocation {
            buffer_space: BufferSpace {
                offset: ib_alloc.offset,
                size: allocation_size,
            },
            index_bytes: BufferSpace {
                offset: ib_alloc.data_offset,
                size: ib_alloc.data_length,
            },
            vertex_bytes: BufferSpace {
                offset: vb_alloc.data_offset,
                size: vb_alloc.data_length,
            },
            vertex_alignment,
        });
        allocation
    }

    fn allocate_temp(
        &mut self,
        index_bytes: u32,
        vertex_bytes: u32,
        vertex_alignment: u32,
    ) -> Option<Allocation> {
        let available_size = self.available_size();

        let vb_alloc = match Self::allocate_back(
            self.tail,
            available_size,
            vertex_bytes,
            vertex_alignment,
        ) {
            Some(alloc) => alloc,
            None => {
                error!(
                    "[VertexBuffer]: available temporary space {} does not have enough space for vertex data",
                    available_size
                );
                return None;
            }
        };

        let ib_alloc = match Self::allocate_back(
            self.tail - vb_alloc.size,
            available_size - vb_alloc.size,
            index_bytes,
            4,
        ) {
            Some(alloc) => alloc,
            None => {
                error!(
                    "[VertexBuffer]: available temporary space {} does not have enough space for index data",
                    available_size
                );
                return None;
            }
        };

        let allocation_size = ib_alloc.size + vb_alloc.size;
        self.tail -= allocation_size;
        Some(Allocation {
            buffer_space: BufferSpace {
                offset: ib_alloc.offset,
                size: allocation_size,
            },
            index_bytes: BufferSpace {
                offset: ib_alloc.data_offset,
                size: ib_alloc.data_length,
            },
            vertex_bytes: BufferSpace {
                offset: vb_alloc.data_offset,
                size: vb_alloc.data_length,
            },
            vertex_alignment,
        })
    }

    fn allocate_front(
        offset: u32,
        available_size: u32,
        length: u32,
        alignment: u32,
    ) -> Option<InternalAllocation> {
        let padding = (alignment - offset % alignment) % alignment;
        if (length + padding) > available_size {
            return None;
        }

        Some(InternalAllocation {
            offset,
            size: padding + length,
            data_padding: padding,
            data_offset: offset + padding,
            data_length: length,
            data_alignment: alignment,
        })
    }

    fn allocate_back(
        offset: u32,
        available_size: u32,
        length: u32,
        alignment: u32,
    ) -> Option<InternalAllocation> {
        assert!(offset >= available_size);
        if available_size < length {
            return None;
        }
        let start = offset - length;
        let padding = start % alignment;
        if available_size < (length + padding) {
            return None;
        }
        let start = start - padding;

        Some(InternalAllocation {
            offset: start,
            size: padding + length,
            data_padding: padding,
            data_offset: start,
            data_length: length,
            data_alignment: alignment,
        })
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Allocation {
    pub buffer_space: BufferSpace,
    pub index_bytes: BufferSpace,
    pub vertex_bytes: BufferSpace,
    pub vertex_alignment: u32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct BufferSpace {
    pub offset: u32,
    pub size: u32,
}

struct InternalAllocation {
    offset: u32,
    size: u32,
    data_padding: u32,
    data_offset: u32,
    data_length: u32,
    data_alignment: u32,
}
