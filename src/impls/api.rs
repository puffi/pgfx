use anyhow::Result;

use crate::{
    common::{
        Attachment, BindlessTextureHandle, BlendState, ClearValue, CullState, DepthState, Format,
        FragmentShaderHandle, Offset, PlatformHandle, PresentMode, RenderTargetDesc,
        RenderTargetHandle, Sampler, ScissorState, Size, StorageBufferHandle, TextureHandle,
        UniformBufferHandle, VertexShaderHandle, Viewport, WindowSize,
    },
    config::Config,
};

pub trait GfxApi {
    type Impl;
    // init
    fn new(platform_handle: PlatformHandle, window_size: WindowSize) -> Result<Self::Impl>;
    fn new_with_config(
        platform_handle: PlatformHandle,
        window_size: WindowSize,
        config: Config,
    ) -> Result<Self::Impl>;

    // context buffer resize
    fn update_window_size(&mut self, size: WindowSize);

    // rendertarget
    fn clear_render_target(&mut self, handle: RenderTargetHandle);
    fn clear_render_target_att_cv(
        &mut self,
        handle: RenderTargetHandle,
        att: Attachment,
        cv: ClearValue,
    );
    fn create_render_target(&mut self, desc: &RenderTargetDesc) -> RenderTargetHandle;
    fn destroy_render_target(&mut self, handle: RenderTargetHandle);
    fn blit_render_target(&mut self, src: RenderTargetHandle, src_attachments: &[Attachment], dst: RenderTargetHandle);

    // draw
    fn present(&mut self, mode: PresentMode);
    fn draw(&mut self, count: u32);
    fn draw_instances_base(&mut self, count: u32, instances: u32, base_instance: u32);

    // shader
    fn create_vertex_shader(&mut self, data: &[u8]) -> VertexShaderHandle;
    fn destroy_vertex_shader(&mut self, handle: VertexShaderHandle);
    fn create_fragment_shader(&mut self, data: &[u8]) -> FragmentShaderHandle;
    fn destroy_fragment_shader(&mut self, handle: FragmentShaderHandle);

    // uniform/storage buffer
    fn create_uniform_buffer(&mut self, size: u32) -> UniformBufferHandle;
    fn destroy_uniform_buffer(&mut self, handle: UniformBufferHandle);
    fn update_uniform_buffer(&mut self, handle: UniformBufferHandle, data: &[u8]);
    fn update_uniform_buffer_offset(
        &mut self,
        handle: UniformBufferHandle,
        data: &[u8],
        offset: u32,
    );
    fn create_storage_buffer(&mut self, size: u32) -> StorageBufferHandle;
    fn destroy_storage_buffer(&mut self, handle: StorageBufferHandle);
    fn update_storage_buffer(&mut self, handle: StorageBufferHandle, data: &[u8]);
    fn update_storage_buffer_offset(
        &mut self,
        handle: StorageBufferHandle,
        data: &[u8],
        offset: u32,
    );

    // texture/sampler
    fn destroy_texture(&mut self, handle: TextureHandle);
    fn create_texture_2d(&mut self, size: Size, fmt: Format, mip_levels: u32) -> TextureHandle;
    fn update_texture(
        &mut self,
        handle: TextureHandle,
        mip_level: u32,
        offset: Offset,
        size: Size,
        data: &[u8],
    );
    fn get_bindless_texture_handle(
        &mut self,
        tex: TextureHandle,
        sampler: Sampler,
    ) -> BindlessTextureHandle;

    // draw state
    fn set_render_targets(&mut self, read: RenderTargetHandle, write: RenderTargetHandle);
    fn set_vertex_shader(&mut self, handle: VertexShaderHandle);
    fn set_fragment_shader(&mut self, handle: FragmentShaderHandle);
    fn set_uniform_buffer(&mut self, handle: UniformBufferHandle, location: u32);
    fn set_uniform_buffer_offset_size(
        &mut self,
        handle: UniformBufferHandle,
        location: u32,
        offset: u32,
        size: u32,
    );
    fn set_storage_buffer(&mut self, handle: StorageBufferHandle, location: u32);
    fn set_storage_buffer_offset_size(
        &mut self,
        handle: StorageBufferHandle,
        location: u32,
        offset: u32,
        size: u32,
    );
    fn set_blend_state(&mut self, blend_state: Option<BlendState>);
    fn set_depth_state(&mut self, depth_state: Option<DepthState>);
    fn set_cull_state(&mut self, cull_state: Option<CullState>);
    fn set_scissor_state(&mut self, scissor_state: Option<ScissorState>);
    fn set_viewport(&mut self, viewport: Viewport);
}
