use super::glad::gl;

pub struct SyncManager {
    sync_objects: Vec<gl::types::GLsync>,
}

impl SyncManager {
    pub fn new(num_frames: usize) -> Self {
        Self {
            sync_objects: vec![std::ptr::null(); num_frames],
        }
    }

    pub fn wait_for_sync_object(&mut self, frame: usize) {
        assert!(self.sync_objects.len() > frame);
        if !self.sync_objects[frame].is_null() {
            let mut status = unsafe {
                gl::ClientWaitSync(self.sync_objects[frame], gl::SYNC_FLUSH_COMMANDS_BIT, 0)
            };
            while (status & gl::ALREADY_SIGNALED) == 0 {
                status = unsafe {
                    gl::ClientWaitSync(self.sync_objects[frame], gl::SYNC_FLUSH_COMMANDS_BIT, 0)
                };
            }
            unsafe {
                gl::DeleteSync(self.sync_objects[frame]);
            }
            self.sync_objects[frame] = std::ptr::null();
        }
    }

    pub fn set_sync_point(&mut self, frame: usize) {
        self.sync_objects[frame] = unsafe { gl::FenceSync(gl::SYNC_GPU_COMMANDS_COMPLETE, 0) };
    }
}
