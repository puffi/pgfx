use crate::{common::StorageBufferHandle, config::MAX_STORAGE_BUFFERS};

use super::buffer::{Access, Buffer};
use log::error;

pub struct StorageBufferManager<'a> {
    storage_buffers: Vec<Option<StorageBuffer<'a>>>,
}

impl<'a> StorageBufferManager<'a> {
    pub fn new() -> Self {
        Self {
            storage_buffers: vec![(); MAX_STORAGE_BUFFERS()]
                .iter()
                .map(|_| None)
                .collect(),
        }
    }

    pub fn create_storage_buffer(&mut self, size: u32) -> StorageBufferHandle {
        let size = if size % 16 == 0 {
            size
        } else {
            size + (16 - size % 16)
        }; // align to 16 bytes

        for (i, sb) in (0u16..).zip(self.storage_buffers.iter_mut()) {
            if sb.is_some() {
                continue;
            }

            let s = StorageBuffer {
                buffer: Buffer::new(size, Access::None, 1),
            };
            *sb = Some(s);

            return StorageBufferHandle::new(i);
        }

        error!("[StorageBufferManager]: Reached storage buffer limit. Can't create a new one");
        StorageBufferHandle::invalid()
    }

    pub fn destroy_storage_buffer(&mut self, handle: StorageBufferHandle) {
        if handle.get_id() > MAX_STORAGE_BUFFERS() as _ {
            error!("[StorageBufferManager]: Destroy storage buffer failed: storage buffer index out of bounds");
            return;
        }

        if self.storage_buffers[handle.get_id() as usize].is_none() {
            error!("[StorageBufferManager]: Destroy storage buffer failed: storage buffer {} is not valid", handle.get_id());
            return;
        }

        self.storage_buffers[handle.get_id() as usize] = None;
    }

    pub fn get_storage_buffer(&self, handle: StorageBufferHandle) -> Option<&StorageBuffer> {
        if handle.get_id() > MAX_STORAGE_BUFFERS() as _ {
            error!("[StorageBufferManager]: Get storage buffer failed: storage buffer index out of bounds");
            return None;
        }

        self.storage_buffers[handle.get_id() as usize].as_ref()
    }
}

#[derive(Default)]
pub struct StorageBuffer<'a> {
    pub buffer: Buffer<'a>,
}
