use crate::{
    common::PlatformHandle,
    config::REQUEST_DEBUG_CONTEXT,
    impls::gl::egl::{
        eglChooseConfig, eglCreateContext, eglCreateWindowSurface, eglQueryString, EGL_EXTENSIONS,
        EGL_NO_CONTEXT,
    },
};

use super::egl::{
    eglBindAPI, eglGetDisplay, eglGetError, eglGetProcAddress, eglInitialize, eglMakeCurrent,
    eglSwapBuffers, eglSwapInterval, eglTerminate, EGLContext, EGLDisplay, EGLNativeDisplayType,
    EGLNativeWindowType, EGLSurface, EGLint, EGL_BLUE_SIZE, EGL_CONFORMANT, EGL_CONTEXT_FLAGS_KHR,
    EGL_CONTEXT_MAJOR_VERSION, EGL_CONTEXT_MAJOR_VERSION_KHR, EGL_CONTEXT_MINOR_VERSION,
    EGL_CONTEXT_MINOR_VERSION_KHR, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR, EGL_CONTEXT_OPENGL_DEBUG,
    EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR, EGL_CONTEXT_OPENGL_PROFILE_MASK,
    EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR, EGL_DEPTH_SIZE, EGL_FALSE, EGL_GREEN_SIZE, EGL_NONE,
    EGL_NO_SURFACE, EGL_OPENGL_API, EGL_OPENGL_BIT, EGL_RED_SIZE, EGL_STENCIL_SIZE, EGL_TRUE,
};
use super::glad::gl;

use log::{debug, error, info};
use thiserror::Error;

const MAX_VERSION: (EGLint, EGLint) = (1, 5);
const MIN_VERSION: (EGLint, EGLint) = (1, 4);

pub struct Context {
    display: EGLDisplay,
    surface: EGLSurface,
    context: EGLContext,
    _native_display: EGLNativeDisplayType,
    _native_window: EGLNativeWindowType,
}

impl Context {
    pub(crate) fn new(platform_handle: PlatformHandle) -> Result<Self, ContextError> {
        let (win, dpy): (EGLNativeWindowType, EGLNativeDisplayType) = match platform_handle {
            PlatformHandle::Wayland {
                display,
                egl_window,
            } => (
                (egl_window as EGLNativeWindowType),
                (display as EGLNativeDisplayType),
            ),
            PlatformHandle::Xlib { window, display } => (
                (window as EGLNativeWindowType),
                (display as EGLNativeDisplayType),
            ),
            PlatformHandle::Win32 {
                hwnd,
                hdc,
                hinstance: _,
            } => ((hwnd as EGLNativeWindowType), (hdc as EGLNativeDisplayType)),
            PlatformHandle::None => unimplemented!(),
        };

        let display;
        {
            let dpy: Option<EGLDisplay> = unsafe { eglGetDisplay(dpy) }.into();
            if let Some(dpy) = dpy {
                display = dpy;
            } else {
                return Err(ContextError::GetDisplay(egl_error_code()));
            }
        }

        let mut major: EGLint = 0;
        let mut minor: EGLint = 0;
        let success = unsafe { eglInitialize(display, &mut major as _, &mut minor as _) };
        // error case
        if success == 0 {
            return Err(ContextError::InitEGL(egl_error_code()));
        } else {
            info!("Initialized EGL version {}.{}", major, minor);
        }

        match (major, minor) {
            (1, 5) => { /* no workaround needed */ }
            (1, 4) => {
                info!("Querying EGL_KHR_get_all_proc_addresses support");
                let exts = unsafe { eglQueryString(display, EGL_EXTENSIONS) };
                let exts = unsafe { std::ffi::CStr::from_ptr(exts) };
                let slice = exts.to_bytes();
                if !slice
                    .windows(b"EGL_KHR_get_all_proc_addresses".len())
                    .any(|w| w == b"EGL_KHR_get_all_proc_addresses")
                {
                    return Err(ContextError::ExtNotSupported(
                        "EGL_KHR_get_all_proc_addresses".into(),
                    ));
                }
            }
            _ => {
                return Err(ContextError::VersionMismatch {
                    major,
                    minor,
                    min_major: MIN_VERSION.0,
                    min_minor: MIN_VERSION.1,
                    max_major: MAX_VERSION.0,
                    max_minor: MAX_VERSION.1,
                });
            }
        }

        let success = unsafe { eglBindAPI(EGL_OPENGL_API as _) };
        if success == 0 {
            return Err(ContextError::BindApi(egl_error_code()));
        }
        info!("Successfully bound API 'OpenGL'");

        let mut config = std::ptr::null_mut();
        let mut num_configs: i32 = 1;
        let success = unsafe {
            eglChooseConfig(
                display,
                SURFACE_ATTRIBS.as_ptr(),
                &mut config,
                1,
                &mut num_configs,
            )
        };
        // TODO query and choose matching config
        if success == 0 {
            return Err(ContextError::ChooseConfigFailed(egl_error_code()));
        }
        if num_configs != 1 {
            return Err(ContextError::NoMatchingConfig);
        }
        info!("Acquired config");

        let surface = unsafe { eglCreateWindowSurface(display, config, win, std::ptr::null()) };
        if surface.is_null() {
            return Err(ContextError::InvalidSurface(egl_error_code()));
        }
        info!("Created surface");

        let context = match (major, minor) {
            (1, 5) => unsafe {
                eglCreateContext(
                    display,
                    config,
                    EGL_NO_CONTEXT,
                    if REQUEST_DEBUG_CONTEXT() {
                        CONTEXT_ATTRIBS_DEBUG.as_ptr()
                    } else {
                        CONTEXT_ATTRIBS.as_ptr()
                    },
                )
            },
            (1, 4) => unsafe {
                eglCreateContext(
                    display,
                    config,
                    EGL_NO_CONTEXT,
                    if REQUEST_DEBUG_CONTEXT() {
                        CONTEXT_ATTRIBS_DEBUG_1_4.as_ptr()
                    } else {
                        CONTEXT_ATTRIBS_1_4.as_ptr()
                    },
                )
            },
            _ => unimplemented!(),
        };

        if context.is_null() {
            return Err(ContextError::InvalidContext(egl_error_code()));
        }

        Ok(Self {
            display,
            surface,
            context,
            _native_display: dpy,
            _native_window: win,
        })
    }

    pub fn make_current(&self) -> bool {
        let success =
            unsafe { eglMakeCurrent(self.display, self.surface, self.surface, self.context) };
        if success == 0 {
            error!(
                "[EGLContext]: Failed to make context current: {:X}",
                unsafe { eglGetError() }
            );
            false
        } else {
            info!("[EGLContext]: Context made current");
            true
        }
    }

    pub fn swap_buffers(&self) {
        unsafe {
            eglSwapBuffers(self.display, self.surface);
        }
    }

    pub fn set_swap_interval(&self, value: i32) {
        unsafe {
            eglSwapInterval(self.display, value);
        }
    }

    pub fn load_gl(&self) {
        gl::load(Self::get_proc_address);
    }

    fn get_proc_address<T: AsRef<str>>(name: T) -> *const std::os::raw::c_void {
        let cstr = std::ffi::CString::new(name.as_ref()).unwrap();
        unsafe { eglGetProcAddress(cstr.as_ptr()) }
    }

    pub fn setup_debug_callback(&self) {
        if REQUEST_DEBUG_CONTEXT() {
            unsafe {
                gl::Enable(gl::DEBUG_OUTPUT);
                gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
                gl::DebugMessageCallback(log_gl_message, std::ptr::null());
            }
        }
    }

    pub fn configure_depth_0_to_1(&self) {
        unsafe {
            gl::ClipControl(gl::LOWER_LEFT, gl::ZERO_TO_ONE);
        }
    }
}

impl Drop for Context {
    fn drop(&mut self) {
        if !self.context.is_null() {
            unsafe {
                eglMakeCurrent(self.display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
            }
            unsafe {
                eglTerminate(self.display);
            }
            info!("Terminating EGL");
        }
    }
}

#[derive(Debug, Error)]
pub(crate) enum ContextError {
    //#[error("Invalid platform handle: {0}")]
    //InvalidPlatformHandle(String),
    //#[error("Invalid display")]
    //InvalidDisplay,
    #[error("Failed to get display: {0:X}")]
    GetDisplay(EGLint),
    #[error("Failed to initialize EGL: {0:X}")]
    InitEGL(EGLint),
    #[error("EGL version mismatch: {major}.{minor}, minimum: {min_major}.{min_minor}, maximum: {max_major}.{max_minor}")]
    VersionMismatch {
        major: EGLint,
        minor: EGLint,
        min_major: EGLint,
        min_minor: EGLint,
        max_major: EGLint,
        max_minor: EGLint,
    },
    #[error("Extension '{0}' not supported")]
    ExtNotSupported(String),
    #[error("Failed to bind API: {0:X}")]
    BindApi(EGLint),
    #[error("Failed to choose config: {0:X}")]
    ChooseConfigFailed(EGLint),
    #[error("No matching config found")]
    NoMatchingConfig,
    #[error("Could not create surface: {0:X}")]
    InvalidSurface(EGLint),
    #[error("Could not create context: {0:X}")]
    InvalidContext(EGLint),
}

fn egl_error_code() -> EGLint {
    unsafe { eglGetError() }
}

const SURFACE_ATTRIBS: &[EGLint] = &[
    EGL_RED_SIZE,
    8,
    EGL_GREEN_SIZE,
    8,
    EGL_BLUE_SIZE,
    8,
    EGL_DEPTH_SIZE,
    24,
    EGL_STENCIL_SIZE,
    8,
    EGL_CONFORMANT,
    EGL_OPENGL_BIT,
    EGL_NONE,
];

const CONTEXT_ATTRIBS: &[EGLint] = &[
    EGL_CONTEXT_MAJOR_VERSION,
    4,
    EGL_CONTEXT_MINOR_VERSION,
    6,
    EGL_CONTEXT_OPENGL_PROFILE_MASK,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
    EGL_CONTEXT_OPENGL_DEBUG,
    EGL_FALSE,
    EGL_NONE,
];

const CONTEXT_ATTRIBS_DEBUG: &[EGLint] = &[
    EGL_CONTEXT_MAJOR_VERSION,
    4,
    EGL_CONTEXT_MINOR_VERSION,
    6,
    EGL_CONTEXT_OPENGL_PROFILE_MASK,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT,
    EGL_CONTEXT_OPENGL_DEBUG,
    EGL_TRUE,
    EGL_NONE,
];

const CONTEXT_ATTRIBS_1_4: &[EGLint] = &[
    EGL_CONTEXT_MAJOR_VERSION_KHR,
    4,
    EGL_CONTEXT_MINOR_VERSION_KHR,
    6,
    EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR,
    EGL_NONE,
];

const CONTEXT_ATTRIBS_DEBUG_1_4: &[EGLint] = &[
    EGL_CONTEXT_MAJOR_VERSION_KHR,
    4,
    EGL_CONTEXT_MINOR_VERSION_KHR,
    6,
    EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR,
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR,
    EGL_CONTEXT_FLAGS_KHR,
    EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR,
    EGL_NONE,
];

extern "system" fn log_gl_message(
    src: gl::types::GLenum,
    ty: gl::types::GLenum,
    id: gl::types::GLuint,
    severity: gl::types::GLenum,
    length: gl::types::GLsizei,
    msg: *const gl::types::GLchar,
    _user_param: *mut std::os::raw::c_void,
) {
    debug!(
        "[GLDebug]: Source: {}, Type: {}, Id: {}, Severity: {}, Length: {}",
        source_to_string(src),
        type_to_string(ty),
        id,
        severity_to_string(severity),
        length
    );
    if let Ok(msg) = unsafe { std::ffi::CStr::from_ptr(msg) }.to_str() {
        debug!("[GLDebug]: {}", msg);
    } else {
        debug!("[GLDebug]: Error message contains invalid UTF-8");
    }
}

fn source_to_string(src: gl::types::GLenum) -> &'static str {
    match src {
        gl::DEBUG_SOURCE_API => "GL_DEBUG_SOURCE_API",
        gl::DEBUG_SOURCE_WINDOW_SYSTEM => "GL_DEBUG_SOURCE_WINDOW_SYSTEM",
        gl::DEBUG_SOURCE_SHADER_COMPILER => "GL_DEBUG_SOURCE_SHADER_COMPILER",
        gl::DEBUG_SOURCE_THIRD_PARTY => "GL_DEBUG_SOURCE_THIRD_PARTY",
        gl::DEBUG_SOURCE_APPLICATION => "GL_DEBUG_SOURCE_APPLICATION",
        gl::DEBUG_SOURCE_OTHER => "GL_DEBUG_SOURCE_OTHER",
        _ => "Unknown source",
    }
}

fn type_to_string(ty: gl::types::GLenum) -> &'static str {
    match ty {
        gl::DEBUG_TYPE_ERROR => "GL_DEBUG_TYPE_ERROR",
        gl::DEBUG_TYPE_DEPRECATED_BEHAVIOR => "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR",
        gl::DEBUG_TYPE_UNDEFINED_BEHAVIOR => "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR",
        gl::DEBUG_TYPE_PORTABILITY => "GL_DEBUG_TYPE_PORTABILITY",
        gl::DEBUG_TYPE_PERFORMANCE => "GL_DEBUG_TYPE_PERFORMANCE",
        gl::DEBUG_TYPE_MARKER => "GL_DEBUG_TYPE_MARKER",
        gl::DEBUG_TYPE_PUSH_GROUP => "GL_DEBUG_TYPE_PUSH_GROUP",
        gl::DEBUG_TYPE_POP_GROUP => "GL_DEBUG_TYPE_POP_GROUP",
        gl::DEBUG_TYPE_OTHER => "GL_DEBUG_TYPE_OTHER",
        _ => "Unknown type",
    }
}

fn severity_to_string(severity: gl::types::GLenum) -> &'static str {
    match severity {
        gl::DEBUG_SEVERITY_HIGH => "GL_DEBUG_SEVERITY_HIGH",
        gl::DEBUG_SEVERITY_MEDIUM => "GL_DEBUG_SEVERITY_MEDIUM",
        gl::DEBUG_SEVERITY_LOW => "GL_DEBUG_SEVERITY_LOW",
        gl::DEBUG_SEVERITY_NOTIFICATION => "GL_DEBUG_SEVERITY_NOTIFICATION",
        _ => "Unknown severity",
    }
}
