use log::{debug, error, info};

use crate::{
    common::{
        AnisotropicFilter, BindlessTextureHandle, Filter, Format, Offset, Sampler, Size,
        TextureHandle, TextureType, Wrap, MAX_SAMPLERS,
    },
    config::MAX_TEXTURES,
    impls::gl::glad::gl,
};

pub type SamplerHandle = u32;

pub struct TextureManager {
    textures: Vec<Option<Texture>>,
    samplers: Vec<Option<SamplerHandle>>,
}

impl Default for TextureManager {
    fn default() -> Self {
        Self::new()
    }
}

impl TextureManager {
    pub fn new() -> Self {
        Self {
            textures: vec![Option::<Texture>::None; MAX_TEXTURES()],
            samplers: vec![Option::<SamplerHandle>::None; MAX_SAMPLERS as usize],
        }
    }

    pub fn create_texture_2d(
        &mut self,
        size: Size,
        fmt: Format,
        mip_levels: u32,
        internal: bool,
    ) -> TextureHandle {
        if size.depth != 1 {
            error!("Depth for 2D texture must be 1");
            return TextureHandle::invalid();
        }
        for (i, tex) in (0u16..).zip(self.textures.iter_mut()) {
            if tex.is_some() {
                continue;
            }

            let mut texture = Texture::new();
            unsafe {
                gl::CreateTextures(gl::TEXTURE_2D, 1, &mut texture.handle);
            }
            unsafe {
                gl::TextureStorage2D(
                    texture.handle,
                    mip_levels as _,
                    fmt.gl_type(),
                    size.width as _,
                    size.height as _,
                );
            }

            texture.ty = TextureType::_2D;
            texture.format = fmt;
            texture.size = size;
            texture.depth = 1;
            texture.mip_levels = mip_levels;
            texture.internal = internal;
            *tex = Some(texture);

            return TextureHandle::new(i);
        }

        info!("[TextureManager]: Texture limit reached, can't allocate a new one");
        TextureHandle::invalid()
    }

    pub fn destroy_texture(&mut self, handle: TextureHandle, allow_internal: bool) {
        if handle.get_id() >= MAX_TEXTURES() as _ {
            error!("[TextureManager]: Destroy texture failed: texture index out of bounds");
            return;
        }

        if let Some(tex) = self.textures[handle.get_id() as usize] {
            if tex.is_internal() && !allow_internal {
                error!("[GL][TextureManager]: Destroy texture failed: destroying internal textures requires `allow_internal` to be set");
                return;
            }
            unsafe {
                gl::DeleteTextures(1, &tex.handle);
            }
        }
        self.textures[handle.get_id() as usize] = None;
    }

    pub fn get_texture(&self, handle: TextureHandle) -> Option<&Texture> {
        if handle.get_id() >= MAX_TEXTURES() as _ {
            debug!("[TextureManager]: Get texture failed: texture index out of bounds");
            return None;
        }

        self.textures[handle.get_id() as usize].as_ref()
        //if !self.textures[handle.get_id() as usize].is_valid() {
        //    debug!("[TextureManager]: Get texture failed: invalid texture");
        //    return None;
        //}

        //Some(&self.textures[handle.get_id() as usize])
    }

    pub fn update_texture(
        &self,
        handle: TextureHandle,
        mip_level: u32,
        offset: Offset,
        size: Size,
        data: &[u8],
    ) {
        if handle.get_id() >= MAX_TEXTURES() as _ {
            error!("[TextureManager]: Update texture failed: texture index out of bounds");
            return;
        }

        if let Some(tex) = self.textures[handle.get_id() as usize] {
            match tex.ty {
                TextureType::_2D => unsafe {
                    gl::TextureSubImage2D(
                        tex.handle,
                        mip_level as _,
                        offset.x as _,
                        offset.y as _,
                        size.width as _,
                        size.height as _,
                        tex.format.gl_format(),
                        tex.format.gl_base_type(),
                        data.as_ptr() as _,
                    );
                },
            }
        } else {
            error!("[TextureManager]: Update texture failed: invalid texture");
        }
    }

    pub fn get_sampler_handle(&mut self, sampler: Sampler) -> SamplerHandle {
        let hash = sampler.get_hash();
        assert!((hash as usize) < self.samplers.len());

        if let Some(handle) = self.samplers[hash as usize] {
            return handle;
        }

        debug!(
            "[SamplerManager]: Creating sampler handle from sampler {}",
            sampler
        );
        let mut handle = 0;
        unsafe {
            gl::CreateSamplers(1, &mut handle);
        }
        unsafe {
            gl::SamplerParameteri(
                handle,
                gl::TEXTURE_MIN_FILTER,
                sampler.get_min_filter().gl_type() as _,
            );
        }
        unsafe {
            gl::SamplerParameteri(
                handle,
                gl::TEXTURE_MAG_FILTER,
                sampler.get_mag_filter().gl_type() as _,
            );
        }
        unsafe {
            gl::SamplerParameterf(
                handle,
                gl::TEXTURE_MAX_ANISOTROPY,
                sampler.get_anisotropic_filter().value() as f32,
            );
        }
        unsafe {
            gl::SamplerParameteri(
                handle,
                gl::TEXTURE_WRAP_S,
                sampler.get_wrap_u().gl_type() as _,
            );
        }
        unsafe {
            gl::SamplerParameteri(
                handle,
                gl::TEXTURE_WRAP_T,
                sampler.get_wrap_v().gl_type() as _,
            );
        }
        unsafe {
            gl::SamplerParameteri(
                handle,
                gl::TEXTURE_WRAP_R,
                sampler.get_wrap_w().gl_type() as _,
            );
        }

        self.samplers[hash as usize] = Some(handle);
        handle
    }

    pub fn get_bindless_texture_handle(
        &mut self,
        tex: TextureHandle,
        sampler: Sampler,
    ) -> BindlessTextureHandle {
        let sampler_handle = self.get_sampler_handle(sampler);

        match self.get_texture(tex) {
            Some(tex) => {
                let handle = unsafe { gl::GetTextureSamplerHandleARB(tex.handle, sampler_handle) };
                if handle != 0 {
                    unsafe {
                        gl::MakeTextureHandleResidentARB(handle);
                    }
                    return BindlessTextureHandle::new(handle as _);
                }
                BindlessTextureHandle::invalid()
            }
            _ => BindlessTextureHandle::invalid(),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Texture {
    ty: TextureType,
    format: Format,
    handle: u32,
    size: Size,
    depth: u32,
    mip_levels: u32,
    internal: bool,
}

impl Texture {
    fn new() -> Self {
        Self {
            ty: TextureType::_2D,
            format: Format::R8,
            handle: 0,
            size: Size {
                width: 0,
                height: 0,
                depth: 0,
            },
            depth: 0,
            mip_levels: 0,
            internal: false,
        }
    }

    pub fn handle(&self) -> u32 {
        self.handle
    }

    pub fn ty(&self) -> TextureType {
        self.ty
    }

    pub fn is_valid(&self) -> bool {
        self.handle != 0
    }

    pub fn size(&self) -> u32 {
        self.size.width * self.size.height * self.size.depth
    }

    pub fn is_internal(&self) -> bool {
        self.internal
    }
}

//#[derive(Debug, Copy, Clone, PartialEq, Eq)]
//pub struct Sampler {
//    handle: u32,
//}
//
//impl Sampler {
//    fn new() -> Self {
//        Self { handle: 0 }
//    }
//
//    fn is_valid(&self) -> bool {
//        self.handle != 0
//    }
//
//    pub fn handle(&self) -> u32 {
//        self.handle
//    }
//}

trait SamplerUtils {
    fn value(&self) -> u16;
    fn gl_type(&self) -> gl::types::GLenum;
}

impl SamplerUtils for Filter {
    fn value(&self) -> u16 {
        match self {
            Self::Nearest => 0,
            Self::Linear => 1,
        }
    }

    fn gl_type(&self) -> gl::types::GLenum {
        match self {
            Self::Nearest => gl::NEAREST,
            Self::Linear => gl::LINEAR,
        }
    }
}

impl SamplerUtils for AnisotropicFilter {
    fn value(&self) -> u16 {
        match self {
            Self::None => 1,
            Self::X4 => 4,
            Self::X8 => 8,
            Self::X16 => 16,
        }
    }

    fn gl_type(&self) -> gl::types::GLenum {
        panic!("No mapping to GL types available");
    }
}

impl SamplerUtils for Wrap {
    fn value(&self) -> u16 {
        match self {
            Self::Repeat => 0,
            Self::MirroredRepeat => 1,
            Self::Clamp => 2,
        }
    }

    fn gl_type(&self) -> gl::types::GLenum {
        match self {
            Self::Repeat => gl::REPEAT,
            Self::MirroredRepeat => gl::MIRRORED_REPEAT,
            Self::Clamp => gl::CLAMP_TO_EDGE,
        }
    }
}

//fn sampler_hash(
//    min: Filter,
//    mag: Filter,
//    anisotropic: AnisotropicFilter,
//    wrap_u: Wrap,
//    wrap_v: Wrap,
//    wrap_w: Wrap,
//) -> u16 {
//    min.value() << FILTER_SHIFT_MIN
//        | mag.value() << FILTER_SHIFT_MAG
//        | anisotropic.value() << ANISOTROPIC_FILTER_SHIFT
//        | wrap_u.value() << WRAP_SHIFT_U
//        | wrap_v.value() << WRAP_SHIFT_V
//        | wrap_w.value() << WRAP_SHIFT_W
//}

trait GlUtils {
    fn gl_type(&self) -> gl::types::GLenum;
    fn gl_format(&self) -> gl::types::GLenum;
    fn gl_base_type(&self) -> gl::types::GLenum;
}

impl GlUtils for Format {
    fn gl_type(&self) -> gl::types::GLenum {
        match self {
            Self::R8 => gl::R8,
            Self::RGB8 => gl::RGB8,
            Self::RGBA8 => gl::RGBA8,
            Self::RGB16F => gl::RGB16F,
            Self::RGBA16F => gl::RGBA16F,
            Self::RGB32F => gl::RGB32F,
            Self::RGBA32F => gl::RGBA32F,
            Self::Depth32 => gl::DEPTH_COMPONENT32F,
            Self::Depth32Stencil8 => gl::DEPTH32F_STENCIL8,
        }
    }

    fn gl_format(&self) -> gl::types::GLenum {
        match self {
            Self::R8 => gl::RED,
            Self::RGB8 => gl::RGB,
            Self::RGBA8 => gl::RGBA,
            Self::RGB16F => gl::RGB,
            Self::RGBA16F => gl::RGBA,
            Self::RGB32F => gl::RGB,
            Self::RGBA32F => gl::RGBA,
            Self::Depth32 => gl::DEPTH_COMPONENT,
            Self::Depth32Stencil8 => gl::DEPTH_STENCIL,
        }
    }

    fn gl_base_type(&self) -> gl::types::GLenum {
        match self {
            Self::R8 => gl::UNSIGNED_BYTE,
            Self::RGB8 => gl::UNSIGNED_BYTE,
            Self::RGBA8 => gl::UNSIGNED_BYTE,
            Self::RGB16F => gl::FLOAT,
            Self::RGBA16F => gl::FLOAT,
            Self::RGB32F => gl::FLOAT,
            Self::RGBA32F => gl::FLOAT,
            Self::Depth32 => gl::FLOAT,
            Self::Depth32Stencil8 => gl::FLOAT_32_UNSIGNED_INT_24_8_REV,
        }
    }
}
