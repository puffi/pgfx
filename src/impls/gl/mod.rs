use anyhow::Result;
use log::error;

use crate::common::{BindlessTextureHandle, StorageBufferHandle};

use self::draw_state_manager::{StorageBinding, UniformBinding};

use super::api::GfxApi;

mod egl;
mod glad;

mod buffer_manager;
mod context;
mod draw_state_manager;
mod rendertarget_manager;
pub mod shader_manager;
mod sync_manager;
pub mod texture_manager;

pub struct Impl {
    context: context::Context,
    buffer_manager: buffer_manager::BufferManager,
    draw_state_manager: draw_state_manager::DrawStateManager,
    rendertarget_manager: rendertarget_manager::RenderTargetManager,
    shader_manager: shader_manager::ShaderManager,
    sync_manager: sync_manager::SyncManager,
    texture_manager: texture_manager::TextureManager,

    window_size: crate::common::WindowSize,

    num_frames: usize,
    current_frame: usize,

    blit_state_buffer: StorageBufferHandle,
}

impl Impl {
    fn update_frame(&mut self) {
        self.current_frame = (self.current_frame + 1) % self.num_frames;
        self.buffer_manager.update_frame(self.current_frame);
        self.sync_manager.wait_for_sync_object(self.current_frame);
    }
}

impl GfxApi for Impl {
    type Impl = Impl;

    fn new(
        platform_handle: crate::common::PlatformHandle,
        window_size: crate::common::WindowSize,
    ) -> Result<Self::Impl> {
        Self::new_with_config(
            platform_handle,
            window_size,
            crate::config::Config::default(),
        )
    }

    fn new_with_config(
        platform_handle: crate::common::PlatformHandle,
        window_size: crate::common::WindowSize,
        config: crate::config::Config,
    ) -> Result<Self::Impl> {
        assert!(crate::config::init_config(config));

        let num_frames: usize = 4;
        let current_frame = 0;

        let context = context::Context::new(platform_handle)?;
        context.make_current();
        context.load_gl();
        context.setup_debug_callback();
        if crate::config::USE_REVERSE_Z() {
            context.configure_depth_0_to_1();
        }

        let mut buffer_manager = buffer_manager::BufferManager::new(num_frames as _);
        let draw_state_manager = draw_state_manager::DrawStateManager::new();
        let mut texture_manager = texture_manager::TextureManager::new();
        let rendertarget_manager =
            rendertarget_manager::RenderTargetManager::new(window_size, &mut texture_manager);
        let shader_manager = shader_manager::ShaderManager::new();
        let sync_manager = sync_manager::SyncManager::new(num_frames);

        let blit_state_buffer = buffer_manager.create_storage_buffer(std::mem::size_of::<BlitState>() as u32);

        Ok(Self {
            context,
            buffer_manager,
            draw_state_manager,
            rendertarget_manager,
            shader_manager,
            sync_manager,
            texture_manager,
            window_size,
            num_frames,
            current_frame,
            blit_state_buffer,
        })
    }

    fn update_window_size(&mut self, size: crate::common::WindowSize) {
        self.window_size = size;
        let texture_mgr = &mut self.texture_manager;
        self.rendertarget_manager
            .update_window_size(self.window_size, texture_mgr);
    }

    fn clear_render_target(&mut self, handle: crate::common::RenderTargetHandle) {
        self.rendertarget_manager.clear_render_target(handle);
    }

    fn clear_render_target_att_cv(
        &mut self,
        handle: crate::common::RenderTargetHandle,
        att: crate::common::Attachment,
        cv: crate::common::ClearValue,
    ) {
        self.rendertarget_manager
            .clear_render_target_attachment_clearvalue(handle, &att, &cv);
    }

    fn create_render_target(
        &mut self,
        desc: &crate::common::RenderTargetDesc,
    ) -> crate::common::RenderTargetHandle {
        let texture_mgr = &mut self.texture_manager;
        self.rendertarget_manager
            .create_render_target(desc, texture_mgr)
    }

    fn destroy_render_target(&mut self, handle: crate::common::RenderTargetHandle) {
        let texture_mgr = &mut self.texture_manager;
        self.rendertarget_manager
            .destroy_render_target(handle, texture_mgr);
    }

    fn blit_render_target(&mut self, src: crate::common::RenderTargetHandle, src_attachments: &[crate::common::Attachment], dst: crate::common::RenderTargetHandle) {
        let mut blit_state = BlitState::default();
        if let Some(rt) = self.rendertarget_manager.get_render_target(src)
        {
            for (i, att) in src_attachments.iter().enumerate() {
                let tex = rt.get_texture_by_attachment(*att);
                if !tex.is_valid() {
                    error!("[Gfx]: failed to get source render target {:?} attachment {:?} texture, can't blit", src, att);
                    return;
                }
                blit_state.texture_handles[i] = self.texture_manager.get_bindless_texture_handle(tex, crate::common::Sampler::new().with_filter(crate::common::Filter::Nearest));
            }
        }
        let blit_state_data = [blit_state];
        let data = unsafe {blit_state_data.align_to::<u8>().1 };
        self.update_storage_buffer(self.blit_state_buffer, data);
        self.set_storage_buffer(self.blit_state_buffer, crate::common::BLIT_STATE_BINDING_LOCATION);
        self.set_render_targets(dst, dst);
        self.draw(3);
    }

    fn set_render_targets(
        &mut self,
        read: crate::common::RenderTargetHandle,
        write: crate::common::RenderTargetHandle,
    ) {
        let read = self.rendertarget_manager.get_render_target(read);
        let write = self.rendertarget_manager.get_render_target(write);
        self.draw_state_manager.set_render_targets(read, write);
    }

    fn present(&mut self, mode: crate::common::PresentMode) {
        match mode {
            crate::common::PresentMode::Immediate => self.context.set_swap_interval(0),
            crate::common::PresentMode::Vsync => self.context.set_swap_interval(1),
        }
        self.rendertarget_manager.blit_default_render_target();

        self.sync_manager.set_sync_point(self.current_frame);

        self.context.swap_buffers();

        self.update_frame();
    }

    fn draw(&mut self, count: u32) {
        self.draw_instances_base(count, 1, 0);
    }

    fn draw_instances_base(&mut self, count: u32, instances: u32, base_instance: u32) {
        self.draw_state_manager
            .draw(instances, base_instance, count);
    }

    fn create_vertex_shader(&mut self, data: &[u8]) -> crate::common::VertexShaderHandle {
        self.shader_manager.create_vertex_shader(data)
    }

    fn destroy_vertex_shader(&mut self, handle: crate::common::VertexShaderHandle) {
        self.shader_manager.destroy_vertex_shader(handle);
    }

    fn create_fragment_shader(&mut self, data: &[u8]) -> crate::common::FragmentShaderHandle {
        self.shader_manager.create_fragment_shader(data)
    }

    fn destroy_fragment_shader(&mut self, handle: crate::common::FragmentShaderHandle) {
        self.shader_manager.destroy_fragment_shader(handle);
    }

    fn create_uniform_buffer(&mut self, size: u32) -> crate::common::UniformBufferHandle {
        self.buffer_manager.create_uniform_buffer(size)
    }

    fn destroy_uniform_buffer(&mut self, handle: crate::common::UniformBufferHandle) {
        self.buffer_manager.destroy_uniform_buffer(handle)
    }

    fn update_uniform_buffer(&mut self, handle: crate::common::UniformBufferHandle, data: &[u8]) {
        self.update_uniform_buffer_offset(handle, data, 0);
    }

    fn update_uniform_buffer_offset(
        &mut self,
        handle: crate::common::UniformBufferHandle,
        data: &[u8],
        offset: u32,
    ) {
        self.buffer_manager
            .update_uniform_buffer(handle, data, offset);
    }

    fn create_storage_buffer(&mut self, size: u32) -> crate::common::StorageBufferHandle {
        self.buffer_manager.create_storage_buffer(size)
    }

    fn destroy_storage_buffer(&mut self, handle: crate::common::StorageBufferHandle) {
        self.buffer_manager.destroy_storage_buffer(handle)
    }

    fn update_storage_buffer(&mut self, handle: crate::common::StorageBufferHandle, data: &[u8]) {
        self.update_storage_buffer_offset(handle, data, 0);
    }

    fn update_storage_buffer_offset(
        &mut self,
        handle: crate::common::StorageBufferHandle,
        data: &[u8],
        offset: u32,
    ) {
        self.buffer_manager
            .update_storage_buffer(handle, data, offset);
    }

    fn destroy_texture(&mut self, handle: crate::common::TextureHandle) {
        self.texture_manager.destroy_texture(handle, false);
    }

    fn create_texture_2d(
        &mut self,
        size: crate::common::Size,
        fmt: crate::common::Format,
        mip_levels: u32,
    ) -> crate::common::TextureHandle {
        self.texture_manager
            .create_texture_2d(size, fmt, mip_levels, false)
    }

    fn update_texture(
        &mut self,
        handle: crate::common::TextureHandle,
        mip_level: u32,
        offset: crate::common::Offset,
        size: crate::common::Size,
        data: &[u8],
    ) {
        self.texture_manager
            .update_texture(handle, mip_level, offset, size, data);
    }

    fn get_bindless_texture_handle(
        &mut self,
        tex: crate::common::TextureHandle,
        sampler: crate::common::Sampler,
    ) -> crate::common::BindlessTextureHandle {
        self.texture_manager
            .get_bindless_texture_handle(tex, sampler)
    }

    fn set_vertex_shader(&mut self, handle: crate::common::VertexShaderHandle) {
        let vs = self.shader_manager.get_vertex_shader(handle);
        self.draw_state_manager.set_vertex_shader(vs);
    }

    fn set_fragment_shader(&mut self, handle: crate::common::FragmentShaderHandle) {
        let fs = self.shader_manager.get_fragment_shader(handle);
        self.draw_state_manager.set_fragment_shader(fs);
    }

    //fn set_texture(&mut self, handle: crate::common::TextureHandle, location: u32) {
    //    let (handle, ty) = match self.texture_manager.get_texture(handle) {
    //        Some(tex) => (tex.handle(), tex.ty()),
    //        None => (0, crate::common::TextureType::_2D),
    //    };
    //    self.draw_state_manager.set_texture(TextureBinding {
    //        location,
    //        handle,
    //        ty,
    //    });
    //}

    //fn set_sampler(&mut self, sampler: crate::common::Sampler, location: u32) {
    //    let handle = self.texture_manager.get_sampler_handle(sampler);
    //    self.draw_state_manager
    //        .set_sampler(SamplerBinding { location, handle });
    //}

    //fn set_texture_sampler(
    //    &mut self,
    //    tex: crate::common::TextureHandle,
    //    sampler: crate::common::Sampler,
    //    location: u32,
    //) {
    //    self.set_texture(tex, location);
    //    self.set_sampler(sampler, location);
    //}

    fn set_uniform_buffer(&mut self, handle: crate::common::UniformBufferHandle, location: u32) {
        self.set_uniform_buffer_offset_size(handle, location, 0, 0);
    }

    fn set_uniform_buffer_offset_size(
        &mut self,
        handle: crate::common::UniformBufferHandle,
        location: u32,
        offset: u32,
        size: u32,
    ) {
        let (handle, size) = match self.buffer_manager.get_uniform_buffer(handle) {
            Some(ub) => (
                ub.buffer.get_handle(),
                if size == 0 { ub.buffer.size() } else { size },
            ),
            None => (0, 0),
        };

        self.draw_state_manager.set_uniform_buffer(UniformBinding {
            location,
            handle,
            offset,
            size,
        });
    }

    fn set_storage_buffer(&mut self, handle: crate::common::StorageBufferHandle, location: u32) {
        self.set_storage_buffer_offset_size(handle, location, 0, 0);
    }

    fn set_storage_buffer_offset_size(
        &mut self,
        handle: crate::common::StorageBufferHandle,
        location: u32,
        offset: u32,
        size: u32,
    ) {
        let (handle, size) = match self.buffer_manager.get_storage_buffer(handle) {
            Some(sb) => (
                sb.buffer.get_handle(),
                if size == 0 { sb.buffer.size() } else { size },
            ),
            None => (0, 0),
        };

        self.draw_state_manager.set_storage_buffer(StorageBinding {
            location,
            handle,
            offset,
            size,
        })
    }

    fn set_blend_state(&mut self, blend_state: Option<crate::common::BlendState>) {
        self.draw_state_manager.set_blend_state(blend_state);
    }

    fn set_depth_state(&mut self, depth_state: Option<crate::common::DepthState>) {
        self.draw_state_manager.set_depth_state(depth_state);
    }

    fn set_cull_state(&mut self, cull_state: Option<crate::common::CullState>) {
        self.draw_state_manager.set_cull_state(cull_state);
    }

    fn set_scissor_state(&mut self, scissor_state: Option<crate::common::ScissorState>) {
        self.draw_state_manager.set_scissor_state(scissor_state);
    }

    fn set_viewport(&mut self, viewport: crate::common::Viewport) {
        self.draw_state_manager.set_viewport(viewport);
    }
}

/// alignment should be sizeof(vec2) in shader, no padding necessary
struct BlitState {
    vertices: [f32; 6],
    texture_handles: [crate::common::BindlessTextureHandle; 11],
}

impl Default for BlitState {
    fn default() -> Self {
        Self {
        vertices: [-1.0, -1.0, 3.0, -1.0, -1.0, 3.0],
        texture_handles: [BindlessTextureHandle::invalid(); 11],
        }
    }
}
