use log::error;

use crate::{
    common::{StorageBufferHandle, UniformBufferHandle},
    config::{MAX_STORAGE_BUFFERS, MAX_UNIFORM_BUFFERS, STAGING_BUFFER_SIZE},
};

use super::glad::gl;

pub struct BufferManager {
    uniform_buffers: Vec<Option<UniformBuffer>>,
    storage_buffers: Vec<Option<StorageBuffer>>,

    write_staging_buffers: Vec<StagingBuffer>,
    read_staging_buffers: Vec<StagingBuffer>,

    num_frames: u32,
    cur_frame: usize,
}

impl BufferManager {
    pub fn new(num_frames: u32) -> Self {
        Self {
            uniform_buffers: vec![(); MAX_UNIFORM_BUFFERS()]
                .iter()
                .map(|_| None)
                .collect(),
            storage_buffers: vec![(); MAX_STORAGE_BUFFERS()]
                .iter()
                .map(|_| None)
                .collect(),
            write_staging_buffers: Vec::new(),
            read_staging_buffers: Vec::new(),
            num_frames,
            cur_frame: 0,
        }
    }

    pub fn create_storage_buffer(&mut self, size: u32) -> StorageBufferHandle {
        let size = if size % 16 == 0 {
            size
        } else {
            size + (16 - size % 16)
        }; // align to 16 bytes

        for (i, sb) in (0u16..).zip(self.storage_buffers.iter_mut()) {
            if sb.is_some() {
                continue;
            }

            let s = StorageBuffer {
                buffer: Buffer::new(size, Access::None, 1),
            };
            *sb = Some(s);

            return StorageBufferHandle::new(i);
        }

        error!("[StorageBufferManager]: Reached storage buffer limit. Can't create a new one");
        StorageBufferHandle::invalid()
    }

    pub fn destroy_storage_buffer(&mut self, handle: StorageBufferHandle) {
        if handle.get_id() > MAX_STORAGE_BUFFERS() as _ {
            error!("[StorageBufferManager]: Destroy storage buffer failed: storage buffer index out of bounds");
            return;
        }

        if self.storage_buffers[handle.get_id() as usize].is_none() {
            error!("[StorageBufferManager]: Destroy storage buffer failed: storage buffer {} is not valid", handle.get_id());
            return;
        }

        self.storage_buffers[handle.get_id() as usize] = None;
    }

    pub fn get_storage_buffer(&self, handle: StorageBufferHandle) -> Option<&StorageBuffer> {
        if handle.get_id() > MAX_STORAGE_BUFFERS() as _ {
            error!("[StorageBufferManager]: Get storage buffer failed: storage buffer index out of bounds");
            return None;
        }

        self.storage_buffers[handle.get_id() as usize].as_ref()
    }

    pub fn update_storage_buffer(&mut self, handle: StorageBufferHandle, data: &[u8], offset: u32) {
        if let Some(sb) = self.get_storage_buffer(handle) {
            if offset + data.len() as u32 > sb.buffer.size() {
                error!("Update storage buffer failed: offset ({:?}) + data length ({:?}) > buffer size ({:?})", offset, data.len(), sb.buffer.size());
                return;
            }

            if data.is_empty() {
                error!(
                    "Trying to upload data of length 0 to storage buffer {:?}",
                    handle
                );
                return;
            }

            self.upload(
                data,
                BufferView {
                    handle: sb.buffer.get_handle(),
                    offset,
                    size: data.len() as _,
                },
            );
        }
    }

    pub fn create_uniform_buffer(&mut self, size: u32) -> UniformBufferHandle {
        let size = if size % 16 == 0 {
            size
        } else {
            size + (16 - size % 16)
        }; // align to 16 bytes

        for (i, ub) in (0u16..).zip(self.uniform_buffers.iter_mut()) {
            if ub.is_some() {
                continue;
            }
            let u = UniformBuffer {
                buffer: Buffer::new(size, Access::None, 1),
            };
            *ub = Some(u);

            return UniformBufferHandle::new(i);
        }

        error!("[UniformBufferManager]: Reached uniform buffer limit. Can't create a new one");
        UniformBufferHandle::invalid()
    }

    pub fn destroy_uniform_buffer(&mut self, handle: UniformBufferHandle) {
        if handle.get_id() > MAX_UNIFORM_BUFFERS() as _ {
            error!("[UniformBufferManager]: Destroy uniform buffer failed: uniform buffer index out of bounds");
            return;
        }

        if self.uniform_buffers[handle.get_id() as usize].is_none() {
            error!("[UniformBufferManager]: Destroy uniform buffer failed: uniform buffer {} is not valid", handle.get_id());
            return;
        }

        self.uniform_buffers[handle.get_id() as usize] = None;
    }

    pub fn get_uniform_buffer(&self, handle: UniformBufferHandle) -> Option<&UniformBuffer> {
        if handle.get_id() > MAX_UNIFORM_BUFFERS() as _ {
            error!("[UniformBufferManager]: Get uniform buffer failed: uniform buffer index out of bounds");
            return None;
        }

        self.uniform_buffers[handle.get_id() as usize].as_ref()
    }

    pub fn update_uniform_buffer(&mut self, handle: UniformBufferHandle, data: &[u8], offset: u32) {
        if let Some(ub) = self.get_uniform_buffer(handle) {
            if offset + data.len() as u32 > ub.buffer.size() {
                error!("Update uniform buffer failed: offset ({:?}) + data length ({:?}) > buffer size ({:?})", offset, data.len(), ub.buffer.size());
                return;
            }

            if data.is_empty() {
                error!(
                    "Trying to upload data of length 0 to uniform buffer {:?}",
                    handle
                );
                return;
            }

            self.upload(
                data,
                BufferView {
                    handle: ub.buffer.get_handle(),
                    offset,
                    size: data.len() as u32,
                },
            );
        }
    }

    pub fn update_frame(&mut self, frame: usize) {
        self.cur_frame = frame;

        self.reset_spaces();
    }

    fn reset_spaces(&mut self) {
        for buffer in self.write_staging_buffers.iter_mut() {
            buffer.free_space[self.cur_frame] = STAGING_BUFFER_SIZE() as _;
        }
    }

    pub fn upload(&mut self, data: &[u8], view: BufferView) {
        if data.len() > STAGING_BUFFER_SIZE() {
            let mut buffer = Buffer::new(data.len() as u32, Access::Write, 1);
            let handle = buffer.get_handle();
            if let Some(mapped_data) = buffer.mapped_range_mut() {
                mapped_data.clone_from_slice(data);
            } else {
                unreachable!();
            }
            unsafe {
                gl::CopyNamedBufferSubData(
                    handle,
                    view.handle,
                    0,
                    view.offset as _,
                    view.size as _,
                );
            }
            return;
        }

        for buffer in self.write_staging_buffers.iter_mut() {
            let space = buffer.allocate(data.len() as u32, self.cur_frame);
            if !space.is_valid() {
                continue;
            }

            let offset;
            if let Some(mapped_data) = buffer.buffer.mapped_range_mut() {
                offset = space.offset as usize + self.cur_frame * STAGING_BUFFER_SIZE();
                let to_offset = offset + space.size as usize;
                mapped_data[offset..to_offset].clone_from_slice(data);
            } else {
                unreachable!();
            }
            unsafe {
                gl::CopyNamedBufferSubData(
                    buffer.buffer.get_handle(),
                    view.handle,
                    offset as _,
                    view.offset as _,
                    space.size as _,
                );
            }
            return;
        }

        self.write_staging_buffers
            .push(StagingBuffer::new(self.num_frames));
        self.upload(data, view);
    }
}

//fn align_to(value: u32, alignment: u32) -> u32 {
//    value + (alignment - (value % alignment))
//}

pub struct Buffer {
    handle: u32,
    size: u32,
    _access: Access,
    frames_buffered: u32,
    mapped_ptr: *mut u8,
}

impl Buffer {
    fn new(size: u32, access: Access, frames_buffered: u32) -> Self {
        let mut handle = 0;
        let total_size = frames_buffered * size;
        let mut mapped_ptr = std::ptr::null_mut();
        unsafe {
            gl::CreateBuffers(1, &mut handle);
            gl::NamedBufferStorage(
                handle,
                total_size as isize,
                std::ptr::null(),
                access.to_gl_bits(),
            );
            match access {
                Access::None => {}
                Access::Read | Access::Write => {
                    mapped_ptr = gl::MapNamedBufferRange(
                        handle,
                        0,
                        total_size as isize,
                        access.to_gl_bits(),
                    ) as *mut u8;
                }
            }
        }
        Self {
            handle,
            size,
            _access: access,
            frames_buffered,
            mapped_ptr,
        }
    }

    pub fn is_valid(&self) -> bool {
        self.handle != 0 && self.size != 0
    }

    pub fn size(&self) -> u32 {
        self.size
    }

    pub fn total_size(&self) -> u32 {
        self.size * self.frames_buffered
    }

    pub fn get_handle(&self) -> u32 {
        self.handle
    }

    fn mapped_range(&self) -> Option<&[u8]> {
        if self.mapped_ptr.is_null() {
            return None;
        }
        unsafe {
            Some(std::slice::from_raw_parts(
                self.mapped_ptr,
                self.total_size() as usize,
            ))
        }
    }

    fn mapped_range_mut(&mut self) -> Option<&mut [u8]> {
        if self.mapped_ptr.is_null() {
            return None;
        }
        unsafe {
            Some(std::slice::from_raw_parts_mut(
                self.mapped_ptr,
                self.total_size() as usize,
            ))
        }
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        if self.is_valid() {
            unsafe {
                // necessary, otherwise GL_INVALID_OPERATION will be generated if not mapped
                if !self.mapped_ptr.is_null() {
                    gl::UnmapNamedBuffer(self.handle);
                }
                gl::DeleteBuffers(1, &self.handle);
            }
        }
    }
}

#[derive(Default)]
enum Access {
    #[default]
    None,
    Read,
    Write,
}

impl Access {
    fn to_gl_bits(&self) -> gl::types::GLenum {
        match self {
            Self::None => 0,
            Self::Read => gl::MAP_READ_BIT | gl::MAP_PERSISTENT_BIT | gl::MAP_COHERENT_BIT,
            Self::Write => gl::MAP_WRITE_BIT | gl::MAP_PERSISTENT_BIT | gl::MAP_COHERENT_BIT,
        }
    }
}

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]
pub struct BufferView {
    pub handle: u32,
    pub offset: u32,
    pub size: u32,
}

impl BufferView {
    fn is_valid(&self) -> bool {
        self.handle != 0 && self.size != 0
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct BufferSpace {
    pub offset: u32,
    pub size: u32,
}

impl BufferSpace {
    pub fn is_valid(&self) -> bool {
        self.size != 0
    }
}

struct StagingBuffer {
    buffer: Buffer,
    free_space: Vec<u32>,
}

impl StagingBuffer {
    fn new(num_frames: u32) -> Self {
        let buffer = Buffer::new(STAGING_BUFFER_SIZE() as _, Access::Write, num_frames);
        let free_space = vec![STAGING_BUFFER_SIZE() as u32; num_frames as _];
        Self { buffer, free_space }
    }

    fn allocate(&mut self, size: u32, frame: usize) -> BufferSpace {
        if self.free_space[frame] < size {
            return BufferSpace { offset: 0, size: 0 };
        }

        let space = BufferSpace {
            offset: STAGING_BUFFER_SIZE() as u32 - self.free_space[frame],
            size,
        };
        self.free_space[frame] -= size;
        space
    }
}

pub struct StorageBuffer {
    pub buffer: Buffer,
}

pub struct UniformBuffer {
    pub buffer: Buffer,
}
