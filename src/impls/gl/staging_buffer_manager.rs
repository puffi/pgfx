use crate::config::STAGING_BUFFER_SIZE;

use super::{
    buffer::{Access, Buffer, BufferSpace, BufferView},
    glad::gl,
};

pub struct StagingBufferManager<'a> {
    write_buffers: Vec<StagingBuffer<'a>>,
    current_frame: usize,
    num_frames: usize,
}

impl<'a> StagingBufferManager<'a> {
    pub fn new(num_frames: usize) -> Self {
        Self {
            write_buffers: Vec::new(),
            current_frame: 0,
            num_frames,
        }
    }

    pub fn update_frame(&mut self, frame: usize) {
        self.current_frame = frame;

        self.reset_spaces();
    }

    fn reset_spaces(&mut self) {
        for buffer in self.write_buffers.iter_mut() {
            buffer.free_space[self.current_frame] = STAGING_BUFFER_SIZE() as _;
        }
    }

    pub fn upload(&mut self, data: &[u8], view: BufferView) {
        if data.len() > STAGING_BUFFER_SIZE() {
            let mut buffer = Buffer::new(data.len() as u32, Access::Write, 1);
            let handle = buffer.get_handle();
            if let Some(mapped_data) = &mut buffer.mapped_data {
                mapped_data.clone_from_slice(data);
            } else {
                unreachable!();
            }
            unsafe {
                gl::CopyNamedBufferSubData(
                    handle,
                    view.handle,
                    0,
                    view.offset as _,
                    view.size as _,
                );
            }
            return;
        }

        for buffer in self.write_buffers.iter_mut() {
            let space = buffer.allocate(data.len() as u32, self.current_frame);
            if !space.is_valid() {
                continue;
            }

            let offset;
            if let Some(mapped_data) = &mut buffer.buffer.mapped_data {
                offset = space.offset as usize + self.current_frame * STAGING_BUFFER_SIZE();
                let to_offset = offset + space.size as usize;
                mapped_data[offset..to_offset].clone_from_slice(data);
            } else {
                unreachable!();
            }
            unsafe {
                gl::CopyNamedBufferSubData(
                    buffer.buffer.get_handle(),
                    view.handle,
                    offset as _,
                    view.offset as _,
                    space.size as _,
                );
            }
            return;
        }

        self.write_buffers.push(StagingBuffer::new(self.num_frames));
        self.upload(data, view);
    }
}

struct StagingBuffer<'a> {
    buffer: Buffer<'a>,
    free_space: Vec<u32>,
}

impl<'a> StagingBuffer<'a> {
    fn new(num_frames: usize) -> Self {
        let buffer = Buffer::new(STAGING_BUFFER_SIZE() as _, Access::Write, num_frames);
        let free_space = vec![STAGING_BUFFER_SIZE() as u32; num_frames];
        Self { buffer, free_space }
    }

    fn allocate(&mut self, size: u32, frame: usize) -> BufferSpace {
        if self.free_space[frame] < size {
            return BufferSpace { offset: 0, size: 0 };
        }

        let space = BufferSpace {
            offset: STAGING_BUFFER_SIZE() as u32 - self.free_space[frame],
            size,
        };
        self.free_space[frame] -= size;
        space
    }
}
