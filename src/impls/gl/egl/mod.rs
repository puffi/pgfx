#![allow(dead_code)]

#[allow(clippy::module_inception)]
mod egl;
mod platform;

pub use egl::*;
pub use platform::*;
