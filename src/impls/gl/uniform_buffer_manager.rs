use crate::{common::UniformBufferHandle, config::MAX_UNIFORM_BUFFERS};

use super::buffer::{Access, Buffer};

use log::error;

pub struct UniformBufferManager<'a> {
    uniform_buffers: Vec<Option<UniformBuffer<'a>>>,
}

impl<'a> UniformBufferManager<'a> {
    pub fn new() -> Self {
        Self {
            uniform_buffers: vec![(); MAX_UNIFORM_BUFFERS()]
                .iter()
                .map(|_| None)
                .collect(),
        }
    }

    pub fn create_uniform_buffer(&mut self, size: u32) -> UniformBufferHandle {
        let size = if size % 16 == 0 {
            size
        } else {
            size + (16 - size % 16)
        }; // align to 16 bytes

        for (i, ub) in (0u16..).zip(self.uniform_buffers.iter_mut()) {
            if ub.is_some() {
                continue;
            }
            let u = UniformBuffer {
                buffer: Buffer::new(size, Access::None, 1),
            };
            *ub = Some(u);

            return UniformBufferHandle::new(i);
        }

        error!("[UniformBufferManager]: Reached uniform buffer limit. Can't create a new one");
        UniformBufferHandle::invalid()
    }

    pub fn destroy_uniform_buffer(&mut self, handle: UniformBufferHandle) {
        if handle.get_id() > MAX_UNIFORM_BUFFERS() as _ {
            error!("[UniformBufferManager]: Destroy uniform buffer failed: uniform buffer index out of bounds");
            return;
        }

        if self.uniform_buffers[handle.get_id() as usize].is_none() {
            error!("[UniformBufferManager]: Destroy uniform buffer failed: uniform buffer {} is not valid", handle.get_id());
            return;
        }

        self.uniform_buffers[handle.get_id() as usize] = None;
    }

    pub fn get_uniform_buffer(&self, handle: UniformBufferHandle) -> Option<&UniformBuffer> {
        if handle.get_id() > MAX_UNIFORM_BUFFERS() as _ {
            error!("[UniformBufferManager]: Get uniform buffer failed: uniform buffer index out of bounds");
            return None;
        }

        self.uniform_buffers[handle.get_id() as usize].as_ref()
    }
}

#[derive(Default)]
pub struct UniformBuffer<'a> {
    pub buffer: Buffer<'a>,
}
