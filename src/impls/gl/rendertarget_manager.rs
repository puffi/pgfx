use log::{debug, error};

use crate::{
    common::{
        Attachment, ClearValue, Format, RenderTargetDesc, RenderTargetHandle, RenderTextureSize,
        Size, TextureHandle, WindowSize,
    },
    config::MAX_RENDER_TARGETS,
    impls::gl::glad::gl,
};

use super::texture_manager::TextureManager;

pub struct RenderTargetManager {
    render_targets: Vec<Option<RenderTarget>>,
    window_size: WindowSize,
}

impl RenderTargetManager {
    pub fn new(window_size: WindowSize, texture_mgr: &mut TextureManager) -> Self {
        let render_targets = vec![Option::<RenderTarget>::None; MAX_RENDER_TARGETS()];

        let mut rt_mgr = Self {
            render_targets,
            window_size,
        };
        assert!(rt_mgr.create_default_render_target(texture_mgr));
        rt_mgr
    }

    pub fn create_render_target(
        &mut self,
        desc: &RenderTargetDesc<'_>,
        texture_mgr: &mut TextureManager,
    ) -> RenderTargetHandle {
        for (i, rt) in (0u16..).zip(self.render_targets.iter_mut()) {
            if rt.is_some() {
                continue;
            }

            if !self.create_render_target_impl(i as _, desc, &[], texture_mgr) {
                error!(
                    "[GL][RenderTargetManager]: Create render target failed: {:?}",
                    i
                );
                return RenderTargetHandle::invalid();
            }

            return RenderTargetHandle::new(i);
        }

        error!("[RenderTargetManager]: No free render target slot available");
        RenderTargetHandle::invalid()
    }

    pub fn destroy_render_target(
        &mut self,
        handle: RenderTargetHandle,
        texture_mgr: &mut TextureManager,
    ) {
        if handle.get_id() >= MAX_RENDER_TARGETS() as _ {
            error!("[RenderTargetManager]: Destroy render target failed: render target index out of range");
            return;
        }

        if self.render_targets[handle.get_id() as usize].is_none() {
            error!("[RenderTargetManager]: Destroy render targer failed: no valid render target");
            return;
        }

        for tex in self.render_targets[handle.get_id() as usize]
            .as_ref()
            .unwrap()
            .texture_handles
            .iter()
        {
            texture_mgr.destroy_texture(*tex, true);
        }

        unsafe {
            gl::DeleteFramebuffers(
                1,
                &self.render_targets[handle.get_id() as usize]
                    .as_mut()
                    .unwrap()
                    .handle,
            );
        }
        self.render_targets[handle.get_id() as usize] = None;
    }

    pub fn get_render_target(&self, handle: RenderTargetHandle) -> Option<&RenderTarget> {
        if handle.get_id() >= MAX_RENDER_TARGETS() as _ {
            error!(
                "[RenderTargetManager]: Get render target failed: render target index out of range"
            );
            return None;
        }

        if self.render_targets[handle.get_id() as usize].is_none() {
            error!("[RenderTargetManager]: Get render target failed: no valid render target found");
            return None;
        }

        self.render_targets[handle.get_id() as usize].as_ref()
    }

    pub fn update_window_size(
        &mut self,
        window_size: WindowSize,
        texture_mgr: &mut TextureManager,
    ) {
        self.window_size = window_size;
        let mut attachments = Vec::new();
        let mut formats = Vec::new();
        let mut clear_values = Vec::new();
        let mut texture_sizes = Vec::new();
        let mut old_texture_handles = Vec::new();
        let mut recreate_current;
        for idx in 0..MAX_RENDER_TARGETS() {
            recreate_current = false;
            if let Some(rt) = &mut self.render_targets[idx] {
                for (att_idx, tex_size) in rt.texture_sizes.iter().enumerate() {
                    match tex_size {
                        RenderTextureSize::Fixed { .. } => continue,
                        RenderTextureSize::Scaled { width, height } => {
                            let new_size = Size {
                                width: (width * window_size.width_pixels as f32) as u32,
                                height: (height * window_size.height_pixels as f32) as u32,
                                depth: 1,
                            };
                            if new_size != rt.current_sizes[att_idx] {
                                debug!(
                                    "[GL][RenderTargetManager]: Resizing render target: {:?}",
                                    idx
                                );
                                recreate_current = true;
                                attachments = std::mem::take(&mut rt.attachments);
                                formats = std::mem::take(&mut rt.formats);
                                clear_values = std::mem::take(&mut rt.clear_values);
                                texture_sizes = std::mem::take(&mut rt.texture_sizes);
                                old_texture_handles = std::mem::take(&mut rt.texture_handles);
                                break;
                            }
                        }
                    }
                }
            }
            if recreate_current {
                let desc = RenderTargetDesc {
                    attachments: &attachments,
                    formats: &formats,
                    clear_values: &clear_values,
                    texture_sizes: &texture_sizes,
                };
                assert!(self.create_render_target_impl(
                    idx,
                    &desc,
                    &old_texture_handles,
                    texture_mgr
                ));
            }
        }
    }

    pub fn blit_default_render_target(&self) {
        let cv = ClearValue::Vec4([0.0, 0.0, 0.0, 1.0]);
        self.clear_render_target_handle_attachment_clearvalue(0, &Attachment::Color0, &cv);

        let current_size = self.render_targets[0].as_ref().unwrap().current_sizes[0];
        unsafe {
            gl::BlitNamedFramebuffer(
                self.render_targets[0].as_ref().unwrap().handle,
                0,
                0,
                0,
                current_size.width as _,
                current_size.height as _,
                0,
                0,
                self.window_size.width_pixels,
                self.window_size.height_pixels,
                gl::COLOR_BUFFER_BIT,
                gl::NEAREST,
            );
        }
    }

    pub fn clear_render_target(&self, handle: RenderTargetHandle) {
        let rt = match self.get_render_target(handle) {
            Some(rt) => rt,
            None => {
                error!(
                    "[RenderTargetManager]: Can't clear invalid render target: {}",
                    handle.get_id()
                );
                return;
            }
        };
        for (att, cv) in rt.attachments.iter().zip(rt.clear_values.iter()) {
            if *cv == ClearValue::None {
                continue;
            }
            self.clear_render_target_handle_attachment_clearvalue(rt.handle, att, cv);
        }
    }

    pub fn clear_render_target_attachment_clearvalue(
        &self,
        handle: RenderTargetHandle,
        att: &Attachment,
        cv: &ClearValue,
    ) {
        let rt = match self.get_render_target(handle) {
            Some(rt) => rt,
            None => {
                error!(
                    "[RenderTargetManager]: Can't clear invalid render target: {}",
                    handle.get_id()
                );
                return;
            }
        };
        self.clear_render_target_handle_attachment_clearvalue(rt.handle, att, cv);
    }

    fn clear_render_target_handle_attachment_clearvalue(
        &self,
        handle: u32,
        att: &Attachment,
        cv: &ClearValue,
    ) {
        match att {
            Attachment::Color0
            | Attachment::Color1
            | Attachment::Color2
            | Attachment::Color3
            | Attachment::Color4
            | Attachment::Color5
            | Attachment::Color6
            | Attachment::Color7 => Self::clear_render_target_color(handle, att, cv),
            Attachment::Depth => {
                if let ClearValue::F32(depth) = cv {
                    Self::clear_render_target_depth(handle, *depth);
                } else {
                    error!("[RenderTargetManager]: Attachment::Depth requires a clear value of type 'F32'");
                }
            }
            Attachment::Stencil => {
                if let ClearValue::I32(stencil) = cv {
                    Self::clear_render_target_stencil(handle, *stencil);
                } else {
                    error!("[RenderTargetManager]: Attachment::Stencil requires a clear value of type 'I32'");
                }
            }
            Attachment::DepthStencil => {
                if let ClearValue::F32I32(depth, stencil) = cv {
                    Self::clear_render_target_depth_stencil(handle, *depth, *stencil);
                } else {
                    error!("[RenderTargetManager]: Attachment::DepthStencil requires a clear value of type 'F32I32'");
                }
            }
        }
    }

    fn clear_render_target_color(handle: u32, att: &Attachment, cv: &ClearValue) {
        match cv {
            ClearValue::None => (),
            ClearValue::F32(val) => unsafe {
                gl::ClearNamedFramebufferfv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    val as _,
                )
            },
            ClearValue::I32(val) => unsafe {
                gl::ClearNamedFramebufferiv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    val as _,
                )
            },
            ClearValue::U32(val) => unsafe {
                gl::ClearNamedFramebufferuiv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    val as _,
                )
            },
            ClearValue::Vec4(val) => unsafe {
                gl::ClearNamedFramebufferfv(
                    handle,
                    gl::COLOR,
                    color_attachment_to_index(att),
                    val as _,
                )
            },
            ClearValue::F32I32(_, _) => {
                error!("[RenderTargetManager]: Wrong clear value type 'F32I32' for color texture");
                //panic!();
            }
        }
    }

    fn clear_render_target_depth(handle: u32, depth: f32) {
        unsafe {
            gl::ClearNamedFramebufferfv(handle, gl::DEPTH, 0, &depth);
        }
    }

    fn clear_render_target_stencil(handle: u32, stencil: i32) {
        unsafe {
            gl::ClearNamedFramebufferiv(handle, gl::STENCIL, 0, &stencil);
        }
    }

    fn clear_render_target_depth_stencil(handle: u32, depth: f32, stencil: i32) {
        unsafe {
            gl::ClearNamedFramebufferfi(handle, gl::DEPTH_STENCIL, 0, depth, stencil);
        }
    }

    fn create_default_render_target(&mut self, texture_mgr: &mut TextureManager) -> bool {
        let attachments = [Attachment::Color0, Attachment::Depth];
        let formats = [Format::RGBA8, Format::Depth32];
        let cvs = [ClearValue::Vec4([0.0, 0.0, 0.0, 0.0]), ClearValue::F32(0.0)];
        let sizes = [
            RenderTextureSize::Scaled {
                width: 1.0,
                height: 1.0,
            },
            RenderTextureSize::Scaled {
                width: 1.0,
                height: 1.0,
            },
        ];

        let desc = RenderTargetDesc {
            attachments: &attachments,
            formats: &formats,
            clear_values: &cvs,
            texture_sizes: &sizes,
        };

        if !self.create_render_target_impl(0, &desc, &[], texture_mgr) {
            return false;
        }

        debug!(
            "[GL][RenderTargetManager]: Created default render target {:?} with index {:?}",
            self.render_targets[0], 0
        );

        true
    }

    fn create_render_target_impl(
        &mut self,
        index: usize,
        desc: &RenderTargetDesc<'_>,
        old_texture_handles: &[TextureHandle],
        texture_mgr: &mut TextureManager,
    ) -> bool {
        if let Some(rt) = &self.render_targets[index] {
            debug!(
                "[GL][RenderTargetManager]: Deleting out of date render target: {:?}",
                index
            );
            for tex in old_texture_handles.iter() {
                texture_mgr.destroy_texture(*tex, true);
            }

            unsafe {
                gl::DeleteFramebuffers(1, &rt.handle);
            }
        }

        let current_sizes = desc
            .texture_sizes
            .iter()
            .map(|rts| match rts {
                RenderTextureSize::Scaled { width, height } => Size {
                    width: (width * self.window_size.width_pixels as f32) as u32,
                    height: (height * self.window_size.height_pixels as f32) as u32,
                    depth: 1,
                },
                RenderTextureSize::Fixed { width, height } => Size {
                    width: *width,
                    height: *height,
                    depth: 1,
                },
            })
            .collect::<Vec<_>>();
        let texture_handles = desc
            .formats
            .iter()
            .enumerate()
            .map(|(idx, fmt)| {
                let size = current_sizes[idx];
                texture_mgr.create_texture_2d(size, *fmt, 1, true)
            })
            .collect::<Vec<_>>();
        let rt = &mut self.render_targets[index];
        *rt = Some(RenderTarget::new(
            desc.attachments,
            desc.formats,
            desc.clear_values,
            desc.texture_sizes,
            &current_sizes,
            &texture_handles,
        ));
        let rt = rt.as_mut().unwrap();
        unsafe {
            gl::CreateFramebuffers(1, &mut rt.handle);
        }

        let mut draw_buffers = Vec::new();
        for i in 0..rt.attachments.len() {
            let att = rt.attachments[i];
            let tex = rt.texture_handles[i];
            let tex_raw_handle = texture_mgr.get_texture(tex).unwrap().handle();
            unsafe {
                gl::NamedFramebufferTexture(rt.handle, attachment_gl_type(&att), tex_raw_handle, 0);
                match att {
                    Attachment::Color0
                    | Attachment::Color1
                    | Attachment::Color2
                    | Attachment::Color3
                    | Attachment::Color4
                    | Attachment::Color5
                    | Attachment::Color6
                    | Attachment::Color7 => draw_buffers.push(attachment_gl_type(&att)),
                    _ => {}
                }
            }
            unsafe {
                gl::NamedFramebufferDrawBuffers(
                    rt.handle,
                    draw_buffers.len() as i32,
                    draw_buffers.as_ptr(),
                );
            }
        }
        true
    }
}

#[derive(Debug, Clone)]
pub struct RenderTarget {
    attachments: Vec<Attachment>,
    formats: Vec<Format>,
    clear_values: Vec<ClearValue>,
    texture_sizes: Vec<RenderTextureSize>,
    current_sizes: Vec<Size>,
    texture_handles: Vec<TextureHandle>,
    handle: u32,
}

impl RenderTarget {
    pub fn new(
        attachments: &[Attachment],
        formats: &[Format],
        clear_values: &[ClearValue],
        texture_sizes: &[RenderTextureSize],
        current_sizes: &[Size],
        texture_handles: &[TextureHandle],
    ) -> Self {
        Self {
            attachments: attachments.into(),
            formats: formats.into(),
            clear_values: clear_values.into(),
            texture_sizes: texture_sizes.into(),
            current_sizes: current_sizes.into(),
            texture_handles: texture_handles.into(),
            handle: 0,
        }
    }

    pub fn get_handle(&self) -> u32 {
        self.handle
    }

    pub fn get_texture_by_attachment(&self, attachment: Attachment) -> TextureHandle {
        self.attachments.iter().position(|att| att == &attachment).and_then(|idx| Some(self.texture_handles[idx])).unwrap_or(TextureHandle::invalid())
    }
}

fn color_attachment_to_index(att: &Attachment) -> i32 {
    match att {
        Attachment::Color0 => 0,
        Attachment::Color1 => 1,
        Attachment::Color2 => 2,
        Attachment::Color3 => 3,
        Attachment::Color4 => 4,
        Attachment::Color5 => 5,
        Attachment::Color6 => 6,
        Attachment::Color7 => 7,
        _ => i32::MAX,
    }
}

fn attachment_gl_type(att: &Attachment) -> gl::GLenum {
    match att {
        Attachment::Color0 => gl::COLOR_ATTACHMENT0,
        Attachment::Color1 => gl::COLOR_ATTACHMENT1,
        Attachment::Color2 => gl::COLOR_ATTACHMENT2,
        Attachment::Color3 => gl::COLOR_ATTACHMENT3,
        Attachment::Color4 => gl::COLOR_ATTACHMENT4,
        Attachment::Color5 => gl::COLOR_ATTACHMENT5,
        Attachment::Color6 => gl::COLOR_ATTACHMENT6,
        Attachment::Color7 => gl::COLOR_ATTACHMENT7,
        Attachment::Depth => gl::DEPTH_ATTACHMENT,
        Attachment::DepthStencil => gl::DEPTH_STENCIL_ATTACHMENT,
        Attachment::Stencil => gl::STENCIL_ATTACHMENT,
    }
}
