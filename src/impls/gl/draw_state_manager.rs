use log::{debug, error};

use crate::{
    common::{
        AttributeType, BlendEquation, BlendFactor, BlendState, CullFrontFace, CullMode, CullState,
        DepthFunc, DepthState, IndexType, ScissorState, TextureType, Viewport,
    },
    config::{MAX_BOUND_STORAGE_BUFFERS, MAX_BOUND_UNIFORM_BUFFERS},
};

use super::{glad::gl, rendertarget_manager::RenderTarget, shader_manager::Shader};

pub struct DrawStateManager {
    vao: u32,
    program_pipeline: u32,
    draw_state: DrawState,
}

impl DrawStateManager {
    pub fn new() -> Self {
        let mut vao = 0;
        let mut program_pipeline = 0;
        unsafe {
            gl::CreateVertexArrays(1, &mut vao);
        }
        unsafe {
            gl::BindVertexArray(vao);
        }
        unsafe {
            gl::CreateProgramPipelines(1, &mut program_pipeline);
        }
        unsafe {
            gl::BindProgramPipeline(program_pipeline);
        }
        Self {
            vao,
            program_pipeline,
            draw_state: DrawState::new(),
        }
    }

    pub fn set_vertex_shader(&mut self, shader: Option<&Shader>) {
        if let Some(shader) = shader {
            if self.draw_state.vertex_shader == shader.get_handle() {
                return;
            }

            debug!(
                "[DrawStateManager]: Binding vertex shader: {}",
                shader.get_handle()
            );
            unsafe {
                gl::UseProgramStages(
                    self.program_pipeline,
                    gl::VERTEX_SHADER_BIT,
                    shader.get_handle(),
                );
            }
            self.draw_state.vertex_shader = shader.get_handle();
        } else {
            if self.draw_state.vertex_shader == 0 {
                return;
            }

            debug!(
                "[DrawStateManager]: Unbinding vertex shader: {}",
                self.draw_state.vertex_shader
            );
            unsafe {
                gl::UseProgramStages(self.program_pipeline, gl::VERTEX_SHADER_BIT, 0);
            }
            self.draw_state.vertex_shader = 0;
        }
    }

    pub fn set_fragment_shader(&mut self, shader: Option<&Shader>) {
        if let Some(shader) = shader {
            if self.draw_state.fragment_shader == shader.get_handle() {
                return;
            }

            debug!(
                "[DrawStateManager]: Binding fragment shader: {}",
                shader.get_handle()
            );
            unsafe {
                gl::UseProgramStages(
                    self.program_pipeline,
                    gl::FRAGMENT_SHADER_BIT,
                    shader.get_handle(),
                );
            }
            self.draw_state.fragment_shader = shader.get_handle();
        } else {
            if self.draw_state.fragment_shader == 0 {
                return;
            }

            debug!(
                "[DrawStateManager]: Unbinding fragment shader: {}",
                self.draw_state.fragment_shader
            );
            unsafe {
                gl::UseProgramStages(self.program_pipeline, gl::FRAGMENT_SHADER_BIT, 0);
            }
            self.draw_state.fragment_shader = 0;
        }
    }

    pub fn set_blend_state(&mut self, blend_state: Option<BlendState>) {
        if self.draw_state.blend_state != blend_state {
            if let Some(blend) = blend_state {
                debug!("[DrawStateManager]: Enabling blending");
                unsafe {
                    gl::Enable(gl::BLEND);
                }
                debug!(
                    "[DrawStateManager]: Setting blend function to: {:?}",
                    blend.func
                );
                unsafe {
                    gl::BlendFunc(blend.func.source.to_gl(), blend.func.destination.to_gl());
                }
                debug!(
                    "[DrawStateManager]: Setting blend equation to: {:?}",
                    blend.equation
                );
                unsafe {
                    gl::BlendEquation(blend.equation.to_gl());
                }
            } else {
                debug!("[DrawStateManager]: Disabling blending");
                unsafe {
                    gl::Disable(gl::BLEND);
                }
            }

            self.draw_state.blend_state = blend_state;
        }
    }

    pub fn set_depth_state(&mut self, depth_state: Option<DepthState>) {
        if self.draw_state.depth_state != depth_state {
            if let Some(depth) = depth_state {
                debug!("[DrawStateManager]: Enabling depth test");
                unsafe {
                    gl::Enable(gl::DEPTH_TEST);
                }
                debug!(
                    "[DrawStateManager]: Setting depth function to: {:?}",
                    depth.func
                );
                unsafe {
                    gl::DepthFunc(depth.func.to_gl());
                }
            } else {
                debug!("[DrawStateManager]: Disabling depth test");
                unsafe {
                    gl::Disable(gl::DEPTH_TEST);
                }
            }

            self.draw_state.depth_state = depth_state;
        }
    }

    pub fn set_cull_state(&mut self, cull_state: Option<CullState>) {
        if self.draw_state.cull_state != cull_state {
            if let Some(cull) = cull_state {
                debug!("[DrawStateManager]: Enable culling");
                unsafe {
                    gl::Enable(gl::CULL_FACE);
                }
                debug!(
                    "[DrawStateManager]: Setting culling mode to: {:?}",
                    cull.mode
                );
                unsafe {
                    gl::CullFace(cull.mode.to_gl());
                }
                debug!(
                    "[DrawStageManager]: Setting culling front face to: {:?}",
                    cull.front_face
                );

                unsafe {
                    gl::FrontFace(cull.front_face.to_gl());
                }
            } else {
                debug!("[DrawStateManager]: Disable culling");
                unsafe {
                    gl::Disable(gl::CULL_FACE);
                }
            }

            self.draw_state.cull_state = cull_state;
        }
    }

    pub fn set_viewport(&mut self, vp: Viewport) {
        if self.draw_state.viewport != vp {
            debug!(
                "[DrawStateManager]: Setting viewport to ({},{},{},{})",
                vp.x, vp.y, vp.width, vp.height
            );
            unsafe {
                gl::Viewport(vp.x, vp.y, vp.width, vp.height);
            }

            self.draw_state.viewport = vp;
        }
    }

    pub fn set_scissor_state(&mut self, scissor_state: Option<ScissorState>) {
        if self.draw_state.scissor_state != scissor_state {
            if let Some(scissor) = scissor_state {
                debug!("[DrawStateManager]: Enable scissor test");
                unsafe {
                    gl::Enable(gl::SCISSOR_TEST);
                }
                debug!("[DrawStateManager]: Setting scissor rect to {:?}", scissor);
                unsafe {
                    gl::Scissor(
                        scissor.x as i32,
                        scissor.y as i32,
                        scissor.width as i32,
                        scissor.height as i32,
                    );
                }
            } else {
                debug!("[DrawStateManager]: Disable scissor test");
                unsafe {
                    gl::Disable(gl::SCISSOR_TEST);
                }
            }
            self.draw_state.scissor_state = scissor_state;
        }
    }

    pub fn set_uniform_buffer(&mut self, binding: UniformBinding) {
        if self.draw_state.uniform_bindings[binding.location as usize] != binding {
            if binding.handle == 0 {
                debug!(
                    "[DrawStateManager]: Unbinding uniform buffer at location: {}",
                    binding.location
                );
            } else {
                debug!("[DrawStateManager]: Binding buffer: {} to uniform buffer location: {} with offset: {} and size: {}", binding.handle, binding.location, binding.offset, binding.size);
            }

            unsafe {
                gl::BindBufferRange(
                    gl::UNIFORM_BUFFER,
                    binding.location,
                    binding.handle,
                    binding.offset as _,
                    binding.size as _,
                );
            }

            self.draw_state.uniform_bindings[binding.location as usize] = binding;
        }
    }

    pub fn set_storage_buffer(&mut self, binding: StorageBinding) {
        if self.draw_state.storage_bindings[binding.location as usize] != binding {
            if binding.handle == 0 {
                debug!(
                    "[DrawStateManager]: Unbinding storage buffer at location: {}",
                    binding.location
                );
            } else {
                debug!("[DrawStateManager]: Binding buffer: {} to storage buffer location: {} with offset: {} and size: {}", binding.handle, binding.location, binding.offset, binding.size);
            }

            unsafe {
                gl::BindBufferRange(
                    gl::SHADER_STORAGE_BUFFER,
                    binding.location,
                    binding.handle,
                    binding.offset as _,
                    binding.size as _,
                );
            }

            self.draw_state.storage_bindings[binding.location as usize] = binding;
        }
    }

    //pub fn set_texture(&mut self, binding: TextureBinding) {
    //    if self.draw_state.texture_bindings[binding.location as usize] != binding {
    //        if binding.handle == 0 {
    //            debug!(
    //                "[DrawStateManager]: Unbinding texture at location: {}",
    //                binding.location
    //            );
    //        } else {
    //            debug!(
    //                "[DrawStateManager]: Binding texture: {:?} of type: {:?} to location: {:?}",
    //                binding.handle, binding.ty, binding.location
    //            );
    //        }
    //        unsafe {
    //            gl::ActiveTexture(gl::TEXTURE0 + binding.location);
    //        }
    //        unsafe {
    //            gl::BindTexture(binding.ty.to_gl(), binding.handle);
    //        }

    //        self.draw_state.texture_bindings[binding.location as usize] = binding;
    //    }
    //}

    //pub fn set_sampler(&mut self, binding: SamplerBinding) {
    //    if self.draw_state.sampler_bindings[binding.location as usize] != binding {
    //        unsafe {
    //            gl::BindSampler(binding.location, binding.handle);
    //        }
    //        self.draw_state.sampler_bindings[binding.location as usize] = binding;
    //    }
    //}

    pub fn set_render_targets(
        &mut self,
        read: Option<&RenderTarget>,
        write: Option<&RenderTarget>,
    ) {
        if let Some(read) = read {
            if read.get_handle() != self.draw_state.read_target {
                debug!(
                    "[DrawStateManager]: Binding read target: {}",
                    read.get_handle()
                );
                self.draw_state.read_target = read.get_handle();
                unsafe {
                    gl::BindFramebuffer(gl::READ_FRAMEBUFFER, read.get_handle());
                }
            }
        } else if self.draw_state.read_target != 0 {
            debug!("[DrawStateManager]: Binding default read target");
            self.draw_state.read_target = 0;
            unsafe {
                gl::BindFramebuffer(gl::READ_FRAMEBUFFER, 0);
            }
        }

        if let Some(write) = write {
            if write.get_handle() != self.draw_state.write_target {
                debug!(
                    "[DrawStateManager]: Binding write target: {}",
                    write.get_handle()
                );
                self.draw_state.write_target = write.get_handle();
                unsafe {
                    gl::BindFramebuffer(gl::DRAW_FRAMEBUFFER, write.get_handle());
                }
            }
        } else if self.draw_state.write_target != 0 {
            debug!("[DrawStateManager]: Binding default write target");
            self.draw_state.write_target = 0;
            unsafe {
                gl::BindFramebuffer(gl::DRAW_FRAMEBUFFER, 0);
            }
        }
    }

    pub fn draw(&self, instances: u32, base_instance: u32, count: u32) {
        if count == 0 {
            error!("[DrawStateManager]: Called draw() with a vertex count of 0");
            return;
        }

        unsafe {
            gl::DrawArraysInstancedBaseInstance(
                gl::TRIANGLES,
                0,
                count as _,
                instances as _,
                base_instance,
            );
        }
    }
}

impl Drop for DrawStateManager {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
        }
        unsafe {
            gl::DeleteProgramPipelines(1, &self.program_pipeline);
        }
    }
}

struct DrawState {
    vertex_shader: u32,
    fragment_shader: u32,

    blend_state: Option<BlendState>,
    depth_state: Option<DepthState>,
    cull_state: Option<CullState>,
    scissor_state: Option<ScissorState>,
    viewport: Viewport,
    uniform_bindings: Vec<UniformBinding>,
    storage_bindings: Vec<StorageBinding>,
    //texture_bindings: Vec<TextureBinding>,
    //sampler_bindings: Vec<SamplerBinding>,
    read_target: u32,
    write_target: u32,
}

impl DrawState {
    fn new() -> Self {
        Self {
            vertex_shader: 0,
            fragment_shader: 0,
            blend_state: None,
            depth_state: None,
            cull_state: None,
            scissor_state: None,
            viewport: Viewport {
                x: 0,
                y: 0,
                width: 0,
                height: 0,
            },
            uniform_bindings: vec![
                UniformBinding {
                    location: 0,
                    handle: 0,
                    offset: 0,
                    size: 0,
                };
                MAX_BOUND_UNIFORM_BUFFERS()
            ],
            storage_bindings: vec![
                StorageBinding {
                    location: 0,
                    handle: 0,
                    offset: 0,
                    size: 0,
                };
                MAX_BOUND_STORAGE_BUFFERS()
            ],
            //texture_bindings: vec![
            //    TextureBinding {
            //        location: 0,
            //        handle: 0,
            //        ty: TextureType::_2D,
            //    };
            //    MAX_BOUND_TEXTURES()
            //],
            //sampler_bindings: vec![
            //    SamplerBinding {
            //        location: 0,
            //        handle: 0,
            //    };
            //    MAX_BOUND_TEXTURES()
            //],
            read_target: 0,
            write_target: 0,
        }
    }
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct UniformBinding {
    pub location: u32,
    pub handle: u32,
    pub offset: u32,
    pub size: u32,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct StorageBinding {
    pub location: u32,
    pub handle: u32,
    pub offset: u32,
    pub size: u32,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct TextureBinding {
    pub location: u32,
    pub handle: u32,
    pub ty: TextureType,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub struct SamplerBinding {
    pub location: u32,
    pub handle: u32,
}

trait ToGlType {
    fn to_gl(&self) -> gl::types::GLenum;
}

impl ToGlType for IndexType {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::U16 => gl::UNSIGNED_SHORT,
            Self::U32 => gl::UNSIGNED_INT,
        }
    }
}

impl ToGlType for TextureType {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::_2D => gl::TEXTURE_2D,
        }
    }
}

impl ToGlType for CullMode {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Front => gl::FRONT,
            Self::Back => gl::BACK,
            Self::FrontAndBack => gl::FRONT_AND_BACK,
        }
    }
}

impl ToGlType for CullFrontFace {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Cw => gl::CW,
            Self::Ccw => gl::CCW,
        }
    }
}

impl ToGlType for DepthFunc {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Never => gl::NEVER,
            Self::Less => gl::LESS,
            Self::Equal => gl::EQUAL,
            Self::LEqual => gl::LEQUAL,
            Self::Greater => gl::GREATER,
            Self::NotEqual => gl::NOTEQUAL,
            Self::GEqual => gl::GEQUAL,
            Self::Always => gl::ALWAYS,
        }
    }
}

impl ToGlType for BlendFactor {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Zero => gl::ZERO,
            Self::One => gl::ONE,
            Self::SrcColor => gl::SRC_COLOR,
            Self::OneMinusSrcColor => gl::ONE_MINUS_SRC_COLOR,
            Self::DstColor => gl::DST_COLOR,
            Self::OneMinusDstColor => gl::ONE_MINUS_DST_COLOR,
            Self::SrcAlpha => gl::SRC_ALPHA,
            Self::OneMinusSrcAlpha => gl::ONE_MINUS_SRC_ALPHA,
            Self::DstAlpha => gl::DST_ALPHA,
            Self::OneMinusDstAlpha => gl::ONE_MINUS_DST_ALPHA,
            Self::ConstantColor => gl::CONSTANT_COLOR,
            Self::OneMinusConstantColor => gl::ONE_MINUS_CONSTANT_COLOR,
            Self::ConstantAlpha => gl::CONSTANT_ALPHA,
            Self::OneMinusConstantAlpha => gl::ONE_MINUS_CONSTANT_ALPHA,
            Self::SrcAlphaSaturate => gl::SRC_ALPHA_SATURATE,
            Self::Src1Color => gl::SRC1_COLOR,
            Self::OneMinusSrc1Color => gl::ONE_MINUS_SRC1_COLOR,
            // 	src1Alpha => gl::SRC1_ALPHA,
            Self::OneMinusSrc1Alpha => gl::ONE_MINUS_SRC1_ALPHA,
        }
    }
}

impl ToGlType for BlendEquation {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::Add => gl::FUNC_ADD,
            Self::Subtract => gl::FUNC_SUBTRACT,
            Self::ReverseSubtract => gl::FUNC_REVERSE_SUBTRACT,
            Self::Min => gl::MIN,
            Self::Max => gl::MAX,
        }
    }
}

impl ToGlType for AttributeType {
    fn to_gl(&self) -> gl::types::GLenum {
        match self {
            Self::None => 0,
            Self::U8 => gl::UNSIGNED_BYTE,
            Self::F32 => gl::FLOAT,
            Self::I32 => gl::INT,
            Self::U32 => gl::UNSIGNED_INT,
        }
    }
}
