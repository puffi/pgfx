pub const SDL_INIT_VIDEO: u32 = 0x00000020;

pub mod error;
pub mod event;
pub mod gesture;
pub mod joystick;
pub mod keyboard;
pub mod keycode;
pub mod mouse;
pub mod scancode;
pub mod syswm;
pub mod touch;
pub mod video;

#[link(name = "SDL2")]
extern "C" {
    pub fn SDL_Init(flags: u32) -> i32;
    pub fn SDL_Quit();
}

#[repr(C)]
pub struct SDL_version {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SDL_bool {
    False,
    True,
}
