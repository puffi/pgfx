use std::{ffi::c_void, os::raw::c_ulong};

use super::{SDL_version, SDL_bool, video::SDL_Window};


#[repr(C)]
pub struct SDL_SysWMinfo {
    pub version: SDL_version,
    pub subsystem: SDL_SYSWM_TYPE,
    pub info: Subsys,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum SDL_SYSWM_TYPE {
    Unknown,
    Windows,
    X11,
    DirectFB,
    Cocoa,
    UIKit,
    Wayland,
    Mir, // no longer available, remove in 2.1
    WinRT,
    Android,
    Vivante,
    OS2,
    Haiku,
    KMSDRM,
    RISCOS,
}


#[repr(C)]
pub union Subsys {
    pub win: Win32,
    pub x11: X11,
    pub wl: Wayland,
    dummy: [u8; 64],
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Win32 {
    pub window: *mut c_void,
    pub hdc: *mut c_void,
    pub hinstance: *mut c_void,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct X11 {
    pub display: *mut c_void,
    pub window: c_ulong,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Wayland {
    pub display: *mut c_void,
    pub surface: *mut c_void,
    pub shell_surface: *mut c_void,
    pub egl_window: *mut c_void,
    pub xdg_surface: *mut c_void,
    pub xdg_toplevel: *mut c_void,
}

extern "C" {
    pub fn SDL_GetWindowWMInfo(win: *mut SDL_Window, info: *mut SDL_SysWMinfo) -> SDL_bool;
}
