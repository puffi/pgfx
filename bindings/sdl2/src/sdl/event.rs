use super::{
    error::mut_c_str,
    gesture::SDL_GestureID,
    joystick::SDL_JoystickID,
    keyboard::SDL_Keysym,
    touch::{SDL_FingerID, SDL_TouchID},
};

pub type SDL_EventType = u32;

pub const SDL_FIRSTEVENT: SDL_EventType = 0;
/**< Unused (do not remove) */

/* Application events */
pub const SDL_QUIT: SDL_EventType = 0x100;
/**< User-requested quit */

/* These application events have special meaning on iOS, see README-ios.md for details */
pub const SDL_APP_TERMINATING: SDL_EventType = 0x101;
/**< The application is being terminated by the OS
     Called on iOS in applicationWillTerminate()
     Called on Android in onDestroy()
*/
pub const SDL_APP_LOWMEMORY: SDL_EventType = 0x102;
/**< The application is low on memory, free memory if possible.
     Called on iOS in applicationDidReceiveMemoryWarning()
     Called on Android in onLowMemory()
*/
pub const SDL_APP_WILLENTERBACKGROUND: SDL_EventType = 0x103;
/**< The application is about to enter the background
     Called on iOS in applicationWillResignActive()
     Called on Android in onPause()
*/
pub const SDL_APP_DIDENTERBACKGROUND: SDL_EventType = 0x104;
/**< The application did enter the background and may not get CPU for some time
     Called on iOS in applicationDidEnterBackground()
     Called on Android in onPause()
*/
pub const SDL_APP_WILLENTERFOREGROUND: SDL_EventType = 0x105;
/**< The application is about to enter the foreground
     Called on iOS in applicationWillEnterForeground()
     Called on Android in onResume()
*/
pub const SDL_APP_DIDENTERFOREGROUND: SDL_EventType = 0x106;
/**< The application is now interactive
     Called on iOS in applicationDidBecomeActive()
     Called on Android in onResume()
*/

pub const SDL_LOCALECHANGED: SDL_EventType = 0x107;
/**< The user's locale preferences have changed. */

/* Display events */
pub const SDL_DISPLAYEVENT: SDL_EventType = 0x150;
/**< Display state change */

/* Window events */
pub const SDL_WINDOWEVENT: SDL_EventType = 0x200;
/**< Window state change */
pub const SDL_SYSWMEVENT: SDL_EventType = 0x201;
/**< System specific event */

/* Keyboard events */
pub const SDL_KEYDOWN: SDL_EventType = 0x300;
/**< Key pressed */
pub const SDL_KEYUP: SDL_EventType = 0x301;
/**< Key released */
pub const SDL_TEXTEDITING: SDL_EventType = 0x302;
/**< Keyboard text editing (composition) */
pub const SDL_TEXTINPUT: SDL_EventType = 0x303;
/**< Keyboard text input */
pub const SDL_KEYMAPCHANGED: SDL_EventType = 0x304;
/**< Keymap changed due to a system event such as an
     input language or keyboard layout change.
*/

/* Mouse events */
pub const SDL_MOUSEMOTION: SDL_EventType = 0x400;
/**< Mouse moved */
pub const SDL_MOUSEBUTTONDOWN: SDL_EventType = 0x401;
/**< Mouse button pressed */
pub const SDL_MOUSEBUTTONUP: SDL_EventType = 0x402;
/**< Mouse button released */
pub const SDL_MOUSEWHEEL: SDL_EventType = 0x403;
/**< Mouse wheel motion */

/* Joystick events */
pub const SDL_JOYAXISMOTION: SDL_EventType = 0x600;
/**< Joystick axis motion */
pub const SDL_JOYBALLMOTION: SDL_EventType = 0x601;
/**< Joystick trackball motion */
pub const SDL_JOYHATMOTION: SDL_EventType = 0x602;
/**< Joystick hat position change */
pub const SDL_JOYBUTTONDOWN: SDL_EventType = 0x603;
/**< Joystick button pressed */
pub const SDL_JOYBUTTONUP: SDL_EventType = 0x604;
/**< Joystick button released */
pub const SDL_JOYDEVICEADDED: SDL_EventType = 0x605;
/**< A new joystick has been inserted into the system */
pub const SDL_JOYDEVICEREMOVED: SDL_EventType = 0x606;
/**< An opened joystick has been removed */

/* Game controller events */
pub const SDL_CONTROLLERAXISMOTION: SDL_EventType = 0x650;
/**< Game controller axis motion */
pub const SDL_CONTROLLERBUTTONDOWN: SDL_EventType = 0x651;
/**< Game controller button pressed */
pub const SDL_CONTROLLERBUTTONUP: SDL_EventType = 0x652;
/**< Game controller button released */
pub const SDL_CONTROLLERDEVICEADDED: SDL_EventType = 0x653;
/**< A new Game controller has been inserted into the system */
pub const SDL_CONTROLLERDEVICEREMOVED: SDL_EventType = 0x654;
/**< An opened Game controller has been removed */
pub const SDL_CONTROLLERDEVICEREMAPPED: SDL_EventType = 0x655;
/**< The controller mapping was updated */
pub const SDL_CONTROLLERTOUCHPADDOWN: SDL_EventType = 0x656;
/**< Game controller touchpad was touched */
pub const SDL_CONTROLLERTOUCHPADMOTION: SDL_EventType = 0x657;
/**< Game controller touchpad finger was moved */
pub const SDL_CONTROLLERTOUCHPADUP: SDL_EventType = 0x658;
/**< Game controller touchpad finger was lifted */
pub const SDL_CONTROLLERSENSORUPDATE: SDL_EventType = 0x659;
/**< Game controller sensor was updated */

/* Touch events */
pub const SDL_FINGERDOWN: SDL_EventType = 0x700;
pub const SDL_FINGERUP: SDL_EventType = 0x701;
pub const SDL_FINGERMOTION: SDL_EventType = 0x702;

/* Gesture events */
pub const SDL_DOLLARGESTURE: SDL_EventType = 0x800;
pub const SDL_DOLLARRECORD: SDL_EventType = 0x801;
pub const SDL_MULTIGESTURE: SDL_EventType = 0x802;

/* Clipboard events */
pub const SDL_CLIPBOARDUPDATE: SDL_EventType = 0x900;
/**< The clipboard changed */

/* Drag and drop events */
pub const SDL_DROPFILE: SDL_EventType = 0x1000;
/**< The system requests a file open */
pub const SDL_DROPTEXT: SDL_EventType = 0x1001;
/**< text/plain drag-and-drop event */
pub const SDL_DROPBEGIN: SDL_EventType = 0x1002;
/**< A new set of drops is beginning (NULL filename) */
pub const SDL_DROPCOMPLETE: SDL_EventType = 0x1003;
/**< Current set of drops is now complete (NULL filename) */

/* Audio hotplug events */
pub const SDL_AUDIODEVICEADDED: SDL_EventType = 0x1100;
/**< A new audio device is available */
pub const SDL_AUDIODEVICEREMOVED: SDL_EventType = 0x1101;
/**< An audio device has been removed. */

/* Sensor events */
pub const SDL_SENSORUPDATE: SDL_EventType = 0x1200;
/**< A sensor was updated */

/* Render events */
pub const SDL_RENDER_TARGETS_RESET: SDL_EventType = 0x2000;
/**< The render targets have been reset and their contents need to be updated */
pub const SDL_RENDER_DEVICE_RESET: SDL_EventType = 0x2001;
/**< The device has been reset and all textures need to be recreated */

/** Events ::SDL_USEREVENT through ::SDL_LASTEVENT are for your use,
 *  and should be allocated with SDL_RegisterEvents()
 */
pub const SDL_USEREVENT: SDL_EventType = 0x8000;

/**
 *  This last event is only for bounding internal arrays
 */
pub const SDL_LASTEVENT: SDL_EventType = 0xFFFF;

#[repr(C)]
pub union SDL_Event {
    pub ty: u32,
    pub common: SDL_CommonEvent,
    pub display: SDL_DisplayEvent,
    pub window: SDL_WindowEvent,
    pub key: SDL_KeyboardEvent,
    pub edit: SDL_TextEditingEvent,
    pub text: SDL_TextInputEvent,
    pub motion: SDL_MouseMotionEvent,
    pub button: SDL_MouseButtonEvent,
    pub wheel: SDL_MouseWheelEvent,
    pub jaxis: SDL_JoyAxisEvent,
    pub jball: SDL_JoyBallEvent,
    pub jhat: SDL_JoyHatEvent,
    pub jbutton: SDL_JoyButtonEvent,
    pub jdevice: SDL_JoyDeviceEvent,
    pub caxis: SDL_ControllerAxisEvent,
    pub cbutton: SDL_ControllerButtonEvent,
    pub cdevice: SDL_ControllerDeviceEvent,
    pub ctouchpad: SDL_ControllerTouchpadEvent,
    pub csensor: SDL_ControllerSensorEvent,
    pub adevice: SDL_AudioDeviceEvent,
    pub sensor: SDL_SensorEvent,
    pub quit: SDL_QuitEvent,
    pub user: SDL_UserEvent,
    pub syswm: SDL_SysWMEvent,
    pub tfinger: SDL_TouchFingerEvent,
    pub mgesture: SDL_MultiGestureEvent,
    pub dgesture: SDL_DollarGestureEvent,
    pub drop: SDL_DropEvent,
    pub padding: [u8; 56],
}

impl Default for SDL_Event {
    fn default() -> Self {
        Self {
            padding: [0u8; 56],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_CommonEvent {
    pub ty: u32,
    pub timestamp: u32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_DisplayEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub display: u32,
    pub event: u8,
    pub padding: [u8; 3],
    pub data1: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_WindowEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub event: u8,
    pub padding: [u8; 3],
    pub data1: i32,
    pub data2: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_KeyboardEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub state: u8,
    pub repeat: u8,
    pub padding: [u8; 2],
    pub keysym: SDL_Keysym,
}

const SDL_TEXTEDITINGEVENT_TEXT_SIZE: usize = 32;

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_TextEditingEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub text: [u8; SDL_TEXTEDITINGEVENT_TEXT_SIZE],
    pub start: i32,
    pub length: i32,
}

const SDL_TEXTINPUTEVENT_TEXT_SIZE: usize = 32;

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_TextInputEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub text: [u8; SDL_TEXTINPUTEVENT_TEXT_SIZE],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_MouseMotionEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub which: u32,
    pub state: u32,
    pub x: i32,
    pub y: i32,
    pub xrel: i32,
    pub yrel: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_MouseButtonEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub which: u32,
    pub button: u8,
    pub state: u8,
    pub clicks: u8,
    pub padding: [u8; 1],
    pub x: i32,
    pub y: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_MouseWheelEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub which: u32,
    pub x: i32,
    pub y: i32,
    pub direction: u32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_JoyAxisEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub axis: u8,
    pub padding: [u8; 3],
    pub value: i16,
    pub padding2: [u16; 1],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_JoyBallEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub ball: u8,
    pub padding: [u8; 3],
    pub xrel: i16,
    pub yrel: i16,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_JoyHatEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub hat: u8,
    pub value: u8,
    pub padding: [u8; 2],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_JoyButtonEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub button: u8,
    pub state: u8,
    pub padding: [u8; 2],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_JoyDeviceEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_ControllerAxisEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub axis: u8,
    pub padding: [u8; 3],
    pub value: i16,
    pub padding2: [u16; 1],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_ControllerButtonEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub button: u8,
    pub state: u8,
    pub padding: [u8; 2],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_ControllerDeviceEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_ControllerTouchpadEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub touchpad: i32,
    pub finger: i32,
    pub x: f32,
    pub y: f32,
    pub pressure: f32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_ControllerSensorEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: SDL_JoystickID,
    pub sensor: i32,
    pub data: [f32; 3],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_AudioDeviceEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: u32,
    pub iscapture: u8,
    pub padding: [u8; 3],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_TouchFingerEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub touchId: SDL_TouchID,
    pub fingerId: SDL_FingerID,
    pub x: f32,
    pub y: f32,
    pub dx: f32,
    pub dy: f32,
    pub pressure: f32,
    pub windowID: u32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_MultiGestureEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub touchId: SDL_TouchID,
    pub dTheta: f32,
    pub dDist: f32,
    pub x: f32,
    pub y: f32,
    pub numFingers: u16,
    pub padding: [u16; 1],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_DollarGestureEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub touchId: SDL_TouchID,
    pub gestureId: SDL_GestureID,
    pub numFingers: u32,
    pub error: f32,
    pub x: f32,
    pub y: f32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_DropEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub file: mut_c_str,
    pub windowID: u32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_SensorEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub which: i32,
    pub data: [f32; 6],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_QuitEvent {
    pub ty: u32,
    pub timestamp: u32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_OSEvent {
    pub ty: u32,
    pub timestamp: u32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_UserEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub windowID: u32,
    pub code: i32,
    pub data1: *mut std::ffi::c_void,
    pub data2: *mut std::ffi::c_void,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_SysWMmsg {
    _private: [u8; 0],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_SysWMEvent {
    pub ty: u32,
    pub timestamp: u32,
    pub msg: *mut SDL_SysWMmsg,
}

pub const SDL_RELEASED: u8 = 0;
pub const SDL_PRESSED: u8 = 1;

extern "C" {
    pub fn SDL_PollEvent(event: *mut SDL_Event) -> i32;
}
