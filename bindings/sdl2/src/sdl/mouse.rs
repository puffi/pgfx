pub type SDL_Button = u8;

const fn SDL_BUTTON(X: SDL_Button) -> SDL_Button {
    1 << ((X) - 1)
}
pub const SDL_BUTTON_LEFT: SDL_Button = 1;
pub const SDL_BUTTON_MIDDLE: SDL_Button = 2;
pub const SDL_BUTTON_RIGHT: SDL_Button = 3;
pub const SDL_BUTTON_X1: SDL_Button = 4;
pub const SDL_BUTTON_X2: SDL_Button = 5;
pub const SDL_BUTTON_LMASK: SDL_Button = SDL_BUTTON(SDL_BUTTON_LEFT);
pub const SDL_BUTTON_MMASK: SDL_Button = SDL_BUTTON(SDL_BUTTON_MIDDLE);
pub const SDL_BUTTON_RMASK: SDL_Button = SDL_BUTTON(SDL_BUTTON_RIGHT);
pub const SDL_BUTTON_X1MASK: SDL_Button = SDL_BUTTON(SDL_BUTTON_X1);
pub const SDL_BUTTON_X2MASK: SDL_Button = SDL_BUTTON(SDL_BUTTON_X2);
