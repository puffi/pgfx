
pub type c_str = *const std::os::raw::c_char;
pub type mut_c_str = *mut std::os::raw::c_char;

extern {
    pub fn SDL_GetError() -> c_str;
}
