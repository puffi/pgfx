use super::error::c_str;
use super::keycode::SDL_Keycode;
use super::scancode::SDL_Scancode;

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct SDL_Keysym {
    pub scancode: SDL_Scancode,
    pub sym: SDL_Keycode,
    pub mod_: u16,
    unused: u32,
}

extern {
    fn SDL_GetKeyFromScancode(scancode: SDL_Scancode) -> SDL_Keycode;
    fn SDL_GetScancodeFromKey(key: SDL_Keycode) -> SDL_Scancode;

    fn SDL_GetScancodeName(scancode: SDL_Scancode) -> c_str;
    fn SDL_GetScancodeFromName(name: c_str) -> SDL_Scancode;
    fn SDL_GetKeyName(key: SDL_Keycode) -> c_str;
    fn SDL_GetKeyFromName(name: c_str) -> SDL_Keycode;
}
