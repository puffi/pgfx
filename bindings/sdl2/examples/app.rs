use sdl2_sys::sdl::{*, event::{SDL_Event, SDL_KEYDOWN, SDL_PollEvent, SDL_WINDOWEVENT}, video::{SDL_CreateWindow, SDL_DestroyWindow, SDL_WINDOWEVENT_CLOSE, SDL_WINDOW_SHOWN, SDL_WINDOW_VULKAN}};

fn main() {
    let res = unsafe { SDL_Init(SDL_INIT_VIDEO) };
    assert!(res == 0, "Failed to initialize");
    println!("Initialized SDL");

    let title = std::ffi::CString::new("bevy_sdl2").unwrap();
    let win = unsafe { SDL_CreateWindow(title.as_ptr(), 0, 0, 1024, 768, SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN) };
    assert!(win != std::ptr::null_mut(), "Window is null");

    let mut quit = false;
    let mut evt = SDL_Event::default();
    while !quit {
        while unsafe { SDL_PollEvent(&mut evt as _) != 0 } {
            if unsafe { evt.ty } == SDL_WINDOWEVENT {
                if unsafe { evt.window.event } == SDL_WINDOWEVENT_CLOSE {
                    quit = true;
                }
            } else if unsafe { evt.ty } == SDL_KEYDOWN {
                
            }
        }
    }

    unsafe { SDL_DestroyWindow(win); }
    unsafe { SDL_Quit(); }
}
